/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.generator

import de.monticore.ModelingLanguageFamily
import de.monticore.io.paths.ModelPath
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.EmbeddedMontiArcLanguage
import de.monticore.lang.monticar.ema2kt.ema2model.MappingException
import de.monticore.lang.monticar.ema2kt.ema2model.ModelBuilder
import de.monticore.lang.monticar.ema2kt.ema2model.ModelBuilderSettings
import de.monticore.lang.monticar.ema2kt.model2kt.DomainModelGenerator
import de.monticore.lang.monticar.ema2kt.model2kt.DomainModelGeneratorException
import de.monticore.lang.monticar.enumlang._symboltable.EnumLangLanguage
import de.monticore.lang.monticar.struct._symboltable.StructLanguage
import de.monticore.lang.monticar.struct._symboltable.StructSymbol
import de.monticore.lang.monticar.struct._symboltable.validation.AllStructReferencesAreRecursivelyResolvableChecker
import de.monticore.symboltable.GlobalScope
import de.monticore.symboltable.Scope
import de.se_rwth.commons.logging.Log
import java.nio.file.Path

internal class GeneratorImplementation(
        private val settings: GeneratorSettings
) : Generator {
    private val languageFamily = ModelingLanguageFamily()
    private val generator: DomainModelGenerator = DomainModelGenerator.create()

    init {
        languageFamily.addModelingLanguage(EmbeddedMontiArcLanguage())
        languageFamily.addModelingLanguage(StructLanguage())
        languageFamily.addModelingLanguage(EnumLangLanguage())
    }

    override fun generate(modelPaths: List<Path>, outputPath: Path) {
        Log.getFindings().clear()
        Log.enableFailQuick(false)
        val mp = ModelPath(modelPaths)
        val symTab = GlobalScope(mp, languageFamily)
        de.monticore.lang.monticar.Utils.addBuiltInTypes(symTab)
        checkErrors()
        SymbolTableInitializer(symTab, modelPaths).initialize()
        checkErrors()
        checkStructureReferences(symTab)
        checkErrors()
        try {
            val modelBuilderSettings = ModelBuilderSettings(
                    settings.packagePrefixParts
            )
            val modelBuilder = ModelBuilder.create(modelBuilderSettings)
            val domainModel = modelBuilder.buildModels(symTab)
            generator.generate(domainModel, outputPath)
        } catch (error: MappingException) {
            throw GeneratorException("error when mapping symbol table onto domain models", error)
        } catch (error: DomainModelGeneratorException) {
            throw GeneratorException("error when generating domain models", error)
        }
    }

    private fun checkStructureReferences(s: Scope) {
        s.localSymbols.flatMap { it.value.toList() }
                .filterIsInstance<StructSymbol>()
                .forEach { AllStructReferencesAreRecursivelyResolvableChecker().isValid(it) }
        s.subScopes.forEach(this::checkStructureReferences)
    }

    private fun checkErrors() {
        val errors = Log.getFindings().filter { it.isError }
        if (errors.isNotEmpty()) {
            val message = "errors occurred: $errors"
            throw GeneratorException(message)
        }
    }
}
