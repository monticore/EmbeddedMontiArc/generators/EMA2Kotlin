/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.generator

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.EmbeddedMontiArcLanguage
import de.monticore.lang.monticar.ema2kt.model.FULLY_QUALIFIED_NAME_SEPARATOR
import de.monticore.lang.monticar.enumlang._symboltable.EnumDeclarationSymbol
import de.monticore.lang.monticar.enumlang._symboltable.EnumLangLanguage
import de.monticore.lang.monticar.struct._symboltable.StructLanguage
import de.monticore.lang.monticar.struct._symboltable.StructSymbol
import de.monticore.symboltable.Scope
import de.monticore.symboltable.Symbol
import de.monticore.symboltable.SymbolKind
import java.io.IOException
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes

internal class SymbolTableInitializer(
        private val symbolTable: Scope,
        private val modelPaths: List<Path>
) {
    fun initialize() {
        modelPaths.forEach {
            val enumsInitializer = EnumSymbolsInitializer(symbolTable, it)
            Files.walkFileTree(it, enumsInitializer)
            val structuresInitializer = StructSymbolsInitializer(symbolTable, it)
            Files.walkFileTree(it, structuresInitializer)
            val componentsInitializer = ComponentSymbolsInitializer(symbolTable, it)
            Files.walkFileTree(it, componentsInitializer)
        }
    }
}

private class EnumSymbolsInitializer(
        symbolTable: Scope,
        basePath: Path
) : AbstractSymbolsInitializer<EnumDeclarationSymbol>(
        symbolTable,
        basePath,
        EnumLangLanguage.FILE_ENDING,
        EnumDeclarationSymbol.KIND
)

private class StructSymbolsInitializer(
        symbolTable: Scope,
        basePath: Path
) : AbstractSymbolsInitializer<StructSymbol>(
        symbolTable,
        basePath,
        StructLanguage.FILE_ENDING,
        StructSymbol.KIND
)

private class ComponentSymbolsInitializer(
        symbolTable: Scope,
        basePath: Path
) : AbstractSymbolsInitializer<ComponentSymbol>(
        symbolTable,
        basePath,
        EmbeddedMontiArcLanguage.FILE_ENDING,
        ComponentSymbol.KIND
)

private val FILE_ENTENSION_DELIMITER = "."

private abstract class AbstractSymbolsInitializer<T : Symbol>(
        private val symbolTable: Scope,
        private val basePath: Path,
        private val fileEnding: String,
        private val symbolKind: SymbolKind
) : SimpleFileVisitor<Path>() {

    @Throws(IOException::class)
    override fun visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult {
        val f = file.toFile()
        if (f.exists() && f.isFile && f.name.toLowerCase().endsWith(fileEnding)) {
            val relativeModelPath = basePath.relativize(file)
            val modelFullyQualifiedName = getModelFullyQualifiedName(relativeModelPath)
            symbolTable.resolve<T>(modelFullyQualifiedName, symbolKind)
        }
        return FileVisitResult.CONTINUE
    }

    open fun getModelFullyQualifiedName(relativeModelPath: Path): String {
        val packageName = relativeModelPath.parent.joinToString(FULLY_QUALIFIED_NAME_SEPARATOR)
        if (packageName.isBlank()) {
            throw GeneratorException(
                    "cannot calculate package name for file: $relativeModelPath"
            )
        }
        val fileNameAndExtension = relativeModelPath.fileName.toString().split(FILE_ENTENSION_DELIMITER)
        if (fileNameAndExtension.size != 2) {
            throw GeneratorException(
                    "cannot calculate model name for file: $relativeModelPath"
            )
        }
        val modelName = fileNameAndExtension[0]
        return "$packageName$FULLY_QUALIFIED_NAME_SEPARATOR$modelName"
    }
}
