/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.generator

import java.nio.file.Path

interface Generator {
    fun generate(modelPaths: List<Path>, outputPath: Path)

    companion object {
        fun create(): Generator {
            return create(GeneratorSettings())
        }

        fun create(settings: GeneratorSettings): Generator {
            return GeneratorImplementation(settings)
        }
    }
}
