/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.generator

data class GeneratorSettings(
        val packagePrefixParts: List<String> = emptyList()
)
