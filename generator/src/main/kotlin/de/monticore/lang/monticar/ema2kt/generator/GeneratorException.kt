/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.generator

class GeneratorException : Exception {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}
