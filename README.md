<!-- (c) https://github.com/MontiCore/monticore -->
[![Build Status](https://travis-ci.org/EmbeddedMontiArc/EMA2Kotlin.svg?branch=master)](https://travis-ci.org/EmbeddedMontiArc/EMA2Kotlin)
[![Build Status](https://circleci.com/gh/EmbeddedMontiArc/EMA2Kotlin/tree/master.svg?style=shield&circle-token=:circle-token)](https://circleci.com/gh/EmbeddedMontiArc/EMA2Kotlin/tree/master)

# EMA2Kotlin
Generates Kotlin code from EMA models.
Can be used thought a Maven plugin (`ema2kt-maven-plugin`) see `ema2kt-maven-plugin-test` project for an example.
