/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.command

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.clearAllOutPorts

internal class ClearAllOutPortsCommand : ComponentCommand {
    override fun execute(component: Component) {
        component.clearAllOutPorts()
    }
}
