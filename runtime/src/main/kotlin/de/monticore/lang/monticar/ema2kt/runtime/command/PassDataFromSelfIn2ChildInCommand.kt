/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.command

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.PortConnection
import de.monticore.lang.monticar.ema2kt.runtime.api.getChild
import de.monticore.lang.monticar.ema2kt.runtime.api.getInPort
import de.monticore.lang.monticar.ema2kt.runtime.passDataTo

internal class PassDataFromSelfIn2ChildInCommand(
        val sourceInPort: String,
        val targetChildName: String,
        val targetInPort: String
) : PortConnection, ComponentCommand {
    override fun execute(component: Component) {
        val source = component.getInPort(sourceInPort)
        val target = component.getChild(targetChildName).getInPort(targetInPort)
        source.passDataTo(target)
    }
}
