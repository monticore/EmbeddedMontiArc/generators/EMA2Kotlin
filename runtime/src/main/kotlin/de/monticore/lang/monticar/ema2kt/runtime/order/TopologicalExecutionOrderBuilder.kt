/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.order

import com.google.common.graph.MutableNetwork
import com.google.common.graph.NetworkBuilder
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.runtime.api.ExecutionException
import de.monticore.lang.monticar.ema2kt.runtime.api.PortConnection
import de.monticore.lang.monticar.ema2kt.runtime.command.ClearAllChildOutPortsCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.ClearAllInPortsCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.ClearAllOutPortsCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.ComponentCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.ExecuteChildCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.PassDataFromChildOut2ChildInCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.PassDataFromChildOut2SelfOutCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.PassDataFromSelfIn2ChildInCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.PassDataFromSelfOut2SelfInCommand

internal class TopologicalExecutionOrderBuilder : ExecutionOrderBuilder {
    override fun buildExecutionOrder(meta: ComponentMetaData): List<ComponentCommand> {
        checkPortConnections(meta.portConnections)
        val unorderedCommands = meta.portConnections.map { it as ComponentCommand }
        val dataFlowGraph = NetworkBuilder
                .directed()
                .allowsParallelEdges(true)
                .build<String, ComponentCommand>()
        dataFlowGraph.addComponentCommands(unorderedCommands)
        val executionOrder = dataFlowGraph
                .sortTopologically()
                .filterNot { it == SPECIAL_NAME_FOR_PARENT_COMPONENT_NODE }
        // 1. clear parent outputs
        val commands = mutableListOf<ComponentCommand>(
                ClearAllOutPortsCommand()
        )
        // 2. execute child components
        executionOrder.forEach { childComponentName ->
            // 2.1 clear child outputs
            commands.add(
                    ClearAllChildOutPortsCommand(childComponentName)
            )
            // 2.2 provide child with data
            dataFlowGraph.predecessors(childComponentName)?.forEach { dataSupplier ->
                val passDataCommands = dataFlowGraph.edgesConnecting(dataSupplier, childComponentName) ?: emptySet()
                commands.addAll(passDataCommands)
            }
            // 2.3 execute child
            commands.add(ExecuteChildCommand(childComponentName))
        }
        // 3. parent inputs are no longer needed at this point therefore they can be cleared
        commands.add(ClearAllInPortsCommand())
        // 4. pass data from children output to parent outputs
        commands.addAll(
                unorderedCommands.filterIsInstance<PassDataFromChildOut2SelfOutCommand>()
        )
        // 5. pass data from outputs to inputs to be used on next execution
        commands.addAll(
                unorderedCommands.filterIsInstance<PassDataFromSelfOut2SelfInCommand>()
        )
        return commands
    }

    private fun checkPortConnections(portConnections: Iterable<PortConnection>) {
        portConnections.forEach {
            when (it) {
                is PassDataFromSelfIn2ChildInCommand,
                is PassDataFromChildOut2ChildInCommand,
                is PassDataFromChildOut2SelfOutCommand,
                is PassDataFromSelfOut2SelfInCommand -> {
                    // do nothing
                }
                else ->
                    throw ExecutionException(
                            "unknown port connection $it"
                    )
            }
        }
    }
}

private val SPECIAL_NAME_FOR_PARENT_COMPONENT_NODE = ""

private fun MutableNetwork<String, ComponentCommand>.addComponentCommands(
        commands: Iterable<ComponentCommand>
) {
    commands.forEach {
        when (it) {
            is PassDataFromSelfIn2ChildInCommand ->
                addEdge(SPECIAL_NAME_FOR_PARENT_COMPONENT_NODE, it.targetChildName, it)
            is PassDataFromChildOut2ChildInCommand ->
                addEdge(it.sourceChildName, it.targetChildName, it)
        }
    }
}
