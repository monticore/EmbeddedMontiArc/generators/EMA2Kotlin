/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.command

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.PortConnection
import de.monticore.lang.monticar.ema2kt.runtime.api.getChild
import de.monticore.lang.monticar.ema2kt.runtime.api.getOutPort
import de.monticore.lang.monticar.ema2kt.runtime.passDataTo

internal class PassDataFromChildOut2SelfOutCommand(
        val sourceChildName: String,
        val sourceOutPort: String,
        val targetOutPort: String
) : PortConnection, ComponentCommand {
    override fun execute(component: Component) {
        val source = component.getChild(sourceChildName).getOutPort(sourceOutPort)
        val target = component.getOutPort(targetOutPort)
        source.passDataTo(target)
    }
}
