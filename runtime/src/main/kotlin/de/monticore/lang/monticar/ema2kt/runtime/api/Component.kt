/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

import de.monticore.lang.monticar.ema2kt.runtime.ComponentSystemImplementation

interface Component {
    val inPorts: Set<Port>
    val outPorts: Set<Port>
    val children: Map<String, Component>
    fun execute()
}

fun Component.toComponentSystem(): ComponentSystem {
    return ComponentSystemImplementation(this)
}

fun Component.getChild(name: String): Component {
    val child = children[name]
    if (child != null) {
        return child
    }
    throw ExecutionException(
            "component does not have child component $name"
    )
}

fun Component.getInPort(name: String): Port {
    return inPorts.getPort("incoming", name)
}

fun Component.getOutPort(name: String): Port {
    return outPorts.getPort("outgoing", name)
}

fun Component.clearAllInPorts() {
    inPorts.clearAll()
}

fun Component.clearAllOutPorts() {
    outPorts.clearAll()
}

private fun Iterable<Port>.clearAll() {
    forEach { it.clear() }
}

private fun Iterable<Port>.getPort(portType: String, name: String): Port {
    val port = find { it.name == name }
    if (port != null) {
        return port
    }
    throw ExecutionException(
            "component does not have $portType port $name"
    )
}
