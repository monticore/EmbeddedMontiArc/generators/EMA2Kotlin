/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentBuilder
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentFactory
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.runtime.api.ExecutionException
import de.monticore.lang.monticar.ema2kt.runtime.api.Port
import de.monticore.lang.monticar.ema2kt.runtime.command.ComponentCommand
import de.monticore.lang.monticar.ema2kt.runtime.order.ExecutionOrderBuilder
import org.slf4j.LoggerFactory

internal class ComponentBuilderImplementation(
        override val factory: ComponentFactory,
        private val executionOrderBuilder: ExecutionOrderBuilder
) : ComponentBuilder {
    companion object {
        private val LOG = LoggerFactory.getLogger(ComponentBuilderImplementation::class.java)
    }

    private val executionOrderCache = mutableMapOf<String, List<ComponentCommand>>()
    private val componentsUnderConstruction = mutableSetOf<String>()

    override fun buildComponent(meta: ComponentMetaData, params: ComponentCreationParameters): Component {
        val c = factory.createComponent(meta.componentId, params)
        if (c != null) {
            LOG.debug("component ${meta.componentId} has been created by factory")
            return c
        }
        if (!componentsUnderConstruction.add(meta.componentId)) {
            throw ExecutionException(
                    "component references form a loop: $componentsUnderConstruction"
            )
        }
        LOG.debug("building component ${meta.componentId}")
        val children = meta.childName2ChildMetaData
                .map { (childName, childMeta) ->
                    val childTypeParameters = getParametersForChildCreation(
                            meta.typeParametersMetaData,
                            params.typeParameters,
                            childName,
                            childMeta.typeParametersMetaData,
                            "type parameter",
                            meta.componentId
                    )
                    val childParameters = getParametersForChildCreation(
                            meta.parametersMetaData,
                            params.parameters,
                            childName,
                            childMeta.parametersMetaData,
                            "parameter",
                            meta.componentId
                    )
                    val childParams = ComponentCreationParameters.create(
                            childTypeParameters,
                            childParameters
                    )
                    childName to buildComponent(childMeta, childParams)
                }
                .toMap()
        val commands = getCommands(meta)
        val component = ComponentImplementation(
                Port.createMany(meta.inPortNames),
                Port.createMany(meta.outPortNames),
                children,
                commands
        )
        componentsUnderConstruction.remove(meta.componentId)
        LOG.debug("component ${meta.componentId} has been built")
        return component
    }

    private fun getCommands(meta: ComponentMetaData): List<ComponentCommand> {
        val cachedCommands = executionOrderCache[meta.componentId]
        if (cachedCommands != null) {
            LOG.debug("retrieved execution commands from cache for component ${meta.componentId}")
            return cachedCommands.toList()
        }
        LOG.debug("building execution commands for component ${meta.componentId}")
        val commands = executionOrderBuilder.buildExecutionOrder(meta)
        executionOrderCache.put(meta.componentId, commands)
        LOG.debug("execution commands for component ${meta.componentId} have been built")
        return commands.toList()
    }
}
