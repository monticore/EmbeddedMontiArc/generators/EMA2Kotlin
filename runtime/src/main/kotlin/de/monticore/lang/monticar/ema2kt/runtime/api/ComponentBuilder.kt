/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

import de.monticore.lang.monticar.ema2kt.runtime.ComponentBuilderImplementation
import de.monticore.lang.monticar.ema2kt.runtime.ComponentFactoryImplementation
import de.monticore.lang.monticar.ema2kt.runtime.order.TopologicalExecutionOrderBuilder

interface ComponentBuilder {
    val factory: ComponentFactory
    fun buildComponent(meta: ComponentMetaData, params: ComponentCreationParameters): Component

    companion object {
        fun create(): ComponentBuilder {
            return create(ComponentFactoryImplementation())
        }

        fun create(factory: ComponentFactory): ComponentBuilder {
            return ComponentBuilderImplementation(factory, TopologicalExecutionOrderBuilder())
        }
    }
}
