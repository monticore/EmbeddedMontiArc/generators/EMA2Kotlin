/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.PassedParameter

internal data class PassedParameterImplementation(
        override val sourceParameterName: String,
        override val targetChildName: String,
        override val targetParameterName: String
) : PassedParameter
