/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.command

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.clearAllInPorts

internal class ClearAllInPortsCommand : ComponentCommand {
    override fun execute(component: Component) {
        component.clearAllInPorts()
    }
}
