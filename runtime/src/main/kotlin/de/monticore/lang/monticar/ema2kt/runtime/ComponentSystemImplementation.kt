/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentSystem
import de.monticore.lang.monticar.ema2kt.runtime.api.getInPort

internal class ComponentSystemImplementation(
        private val component: Component
) : ComponentSystem {
    override fun execute(inputs: Map<String, Any?>): Map<String, Any?> {
        inputs.forEach { portName, value ->
            component.getInPort(portName).value = value
        }
        component.execute()
        return component.outPorts.map { it.name to it.value }.toMap()
    }
}
