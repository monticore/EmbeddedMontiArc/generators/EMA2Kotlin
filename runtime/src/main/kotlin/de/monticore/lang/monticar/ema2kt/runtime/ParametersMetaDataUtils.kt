/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.ExecutionException
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.monticore.lang.monticar.ema2kt.runtime.api.ParametersMetaData

internal fun <T> getParametersForChildCreation(
        parentParametersMeta: ParametersMetaData<T>,
        parentInitializationParameters: List<Parameter<T>>,
        childName: String,
        childParametersMeta: ParametersMetaData<T>,
        nameOfParameter: String,
        parentComponentId: String
): List<Parameter<T>> {
    return childParametersMeta.parameterNames.map { childParamName ->
        parentParametersMeta.getParameterForChild(
                parentInitializationParameters,
                childName,
                childParametersMeta,
                childParamName
        ) ?: throw ExecutionException(
                "cannot instantiate $childName in $parentComponentId:" +
                        " $nameOfParameter $childParamName is not provided"
        )
    }
}

internal fun <T> ParametersMetaData<T>.getParameterForChild(
        parentInitializationParameters: List<Parameter<T>>,
        childName: String,
        childParametersMetaData: ParametersMetaData<T>,
        childParameterName: String
): Parameter<T>? {
    val p = getParameterForChild(parentInitializationParameters, childName, childParameterName)
    if (p != null) {
        return p
    }
    return childParametersMetaData.defaultValuesOfParameters.find { it.name == childParameterName }
}

internal fun <T> ParametersMetaData<T>.getParameterForChild(
        parentInitializationParameters: List<Parameter<T>>,
        childName: String,
        childParameterName: String
): Parameter<T>? {
    val literalParams = literalInitializationParametersForChildren[childName]
    if (literalParams != null) {
        val literalParam = literalParams.find { it.name == childParameterName }
        if (literalParam != null) {
            return literalParam
        }
    }
    val parentParamName = parametersPassed2Children.find {
        it.targetChildName == childName && it.targetParameterName == childParameterName
    }?.sourceParameterName
    if (parentParamName != null) {
        val initParam = parentInitializationParameters.find { it.name == parentParamName }
        if (initParam != null) {
            return initParam
        }
        val defaultParam = defaultValuesOfParameters.find { it.name == parentParamName }
        if (defaultParam != null) {
            return defaultParam
        }
    }
    return null
}
