/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

import de.monticore.lang.monticar.ema2kt.runtime.command.PassDataFromChildOut2ChildInCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.PassDataFromChildOut2SelfOutCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.PassDataFromSelfIn2ChildInCommand
import de.monticore.lang.monticar.ema2kt.runtime.command.PassDataFromSelfOut2SelfInCommand
import de.monticore.lang.monticar.ema2kt.runtime.requireNotBlank

interface PortConnection {
    companion object {
        fun self2Child(
                sourceInPort: String,
                targetChildName: String,
                targetInPort: String
        ): PortConnection {
            sourceInPort.requireNotBlank("source port is blank")
            targetChildName.requireNotBlank("child is blank")
            targetInPort.requireNotBlank("target port is blank")
            return PassDataFromSelfIn2ChildInCommand(sourceInPort, targetChildName, targetInPort)
        }

        fun child2Child(
                sourceChildName: String,
                sourceOutPort: String,
                targetChildName: String,
                targetInPort: String
        ): PortConnection {
            sourceChildName.requireNotBlank("source child is blank")
            sourceOutPort.requireNotBlank("source port is blank")
            targetChildName.requireNotBlank("target child is blank")
            targetInPort.requireNotBlank("target port is blank")
            return PassDataFromChildOut2ChildInCommand(sourceChildName, sourceOutPort, targetChildName, targetInPort)
        }

        fun child2Self(
                sourceChildName: String,
                sourceOutPort: String,
                targetOutPort: String
        ): PortConnection {
            sourceChildName.requireNotBlank("source child is blank")
            sourceOutPort.requireNotBlank("source port is blank")
            targetOutPort.requireNotBlank("target port is blank")
            return PassDataFromChildOut2SelfOutCommand(sourceChildName, sourceOutPort, targetOutPort)
        }

        fun selfOut2SelfIn(
                sourceOutPort: String,
                targetInPort: String
        ): PortConnection {
            sourceOutPort.requireNotBlank("source port is blank")
            targetInPort.requireNotBlank("target port is blank")
            return PassDataFromSelfOut2SelfInCommand(sourceOutPort, targetInPort)
        }
    }
}
