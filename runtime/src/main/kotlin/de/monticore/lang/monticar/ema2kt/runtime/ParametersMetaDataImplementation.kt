/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.monticore.lang.monticar.ema2kt.runtime.api.ParametersMetaData
import de.monticore.lang.monticar.ema2kt.runtime.api.PassedParameter

internal class ParametersMetaDataImplementation<out T>(
        override val parameterNames: Set<String> = emptySet(),
        override val defaultValuesOfParameters: List<Parameter<T>> = emptyList(),
        override val parametersPassed2Children: List<PassedParameter> = emptyList(),
        override val literalInitializationParametersForChildren: Map<String, List<Parameter<T>>> = emptyMap()
) : ParametersMetaData<T>
