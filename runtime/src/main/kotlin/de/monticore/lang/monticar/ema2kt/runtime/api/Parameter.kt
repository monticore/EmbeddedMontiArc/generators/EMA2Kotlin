/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

import de.monticore.lang.monticar.ema2kt.runtime.ParameterImplementation

interface Parameter<out T> {
    val name: String
    val value: T?

    companion object {
        fun <T> create(name: String, value: T): Parameter<T> {
            return ParameterImplementation(name, value)
        }
    }
}
