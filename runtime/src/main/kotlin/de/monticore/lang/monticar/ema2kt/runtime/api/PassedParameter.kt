/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

import de.monticore.lang.monticar.ema2kt.runtime.PassedParameterImplementation

interface PassedParameter {
    val sourceParameterName: String
    val targetChildName: String
    val targetParameterName: String

    companion object {
        fun create(
                sourceParameterName: String,
                targetChildName: String,
                targetParameterName: String
        ): PassedParameter {
            return PassedParameterImplementation(sourceParameterName, targetChildName, targetParameterName)
        }
    }
}
