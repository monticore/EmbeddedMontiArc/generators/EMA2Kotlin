/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

typealias ComponentFactoryFunction = (params: ComponentCreationParameters) -> Component

interface ComponentFactory {
    fun registerFactoryFunction(componentId: String, f: ComponentFactoryFunction)
    fun isFactoryFunctionRegistered(componentId: String): Boolean
    fun createComponent(componentId: String, params: ComponentCreationParameters): Component?
}
