/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.Port
import de.monticore.lang.monticar.ema2kt.runtime.command.ComponentCommand

internal class ComponentImplementation(
        override val inPorts: Set<Port>,
        override val outPorts: Set<Port>,
        override val children: Map<String, Component>,
        private val executionCommands: List<ComponentCommand> = emptyList()
) : Component {
    override fun execute() {
        executionCommands.forEach {
            it.execute(this)
        }
    }
}
