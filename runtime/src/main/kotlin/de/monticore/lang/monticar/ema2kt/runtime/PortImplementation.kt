/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.Port

internal data class PortImplementation(
        override val name: String,
        override var value: Any? = null
) : Port {
    override fun clear() {
        value = null
    }
}

internal fun Port.passDataTo(target: Port) {
    val data = value
    target.value = data
}
