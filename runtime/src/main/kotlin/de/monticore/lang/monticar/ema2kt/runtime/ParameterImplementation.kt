/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter

internal class ParameterImplementation<out T>(
        override val name: String,
        override val value: T?
) : Parameter<T>
