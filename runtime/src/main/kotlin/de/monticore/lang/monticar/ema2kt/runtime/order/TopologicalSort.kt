/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.order

import com.google.common.graph.Network
import de.monticore.lang.monticar.ema2kt.runtime.api.ExecutionException
import java.util.LinkedList

internal fun <N> Network<N, *>.sortTopologically(): List<N> {
    val nodeState = mutableMapOf<N, NodeState>()
    val nodeParent = mutableMapOf<N, N>()
    val nodesInTopologicalOrder = LinkedList<N>()

    fun dfs(node: N) {
        nodeState[node] = NodeState.GRAY
        successors(node)?.forEach {
            when (nodeState[it]) {
                NodeState.WHITE -> {
                    nodeParent.put(it, node)
                    dfs(it)
                }
                NodeState.GRAY -> {
                    val loop = LoopFinder(nodeParent, node, it).findLoop()
                    throw ExecutionException("impossible to sort topologically because of the loop: $loop")
                }
                NodeState.BLACK -> {
                    // already explored -- do nothing
                }
            }
        }
        nodeState[node] = NodeState.BLACK
        nodesInTopologicalOrder.addFirst(node)
    }

    val nodes = nodes() ?: return emptyList()
    nodes.forEach { nodeState[it] = NodeState.WHITE }
    nodes.forEach {
        if (nodeState[it] == NodeState.WHITE) {
            dfs(it)
        }
    }
    return nodesInTopologicalOrder.toList()
}

private enum class NodeState {
    WHITE,
    GRAY,
    BLACK
}

private class LoopFinder<N>(
        private val nodeParent: Map<N, N>,
        private val nodeAtWhichLoopHasBeenDetected: N,
        private val nodeAtWhichLoopStarts: N
) {
    fun findLoop(): List<N> {
        val loop = LinkedList<N>()
        var node: N? = nodeAtWhichLoopHasBeenDetected
        while (node != null && node != nodeAtWhichLoopStarts) {
            loop.addFirst(node)
            node = nodeParent[node]
        }
        loop.addFirst(nodeAtWhichLoopStarts)
        return loop
    }
}
