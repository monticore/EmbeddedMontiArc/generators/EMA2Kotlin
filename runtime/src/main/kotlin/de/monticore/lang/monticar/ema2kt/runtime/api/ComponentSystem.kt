/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

interface ComponentSystem {
    fun execute(inputs: Map<String, Any?>): Map<String, Any?>
}
