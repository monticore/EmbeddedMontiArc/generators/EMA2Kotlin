/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

class ExecutionException(message: String) : RuntimeException(message)
