/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

abstract class AbstractComponentMetaData(
        override val componentId: String,
        override val inPortNames: Set<String>,
        override val outPortNames: Set<String>,
        override val typeParametersMetaData: ParametersMetaData<String> = ParametersMetaData.create(),
        override val parametersMetaData: ParametersMetaData<Any> = ParametersMetaData.create(),
        override val childName2ChildMetaData: Map<String, ComponentMetaData> = emptyMap(),
        override val portConnections: List<PortConnection> = emptyList()
) : ComponentMetaData
