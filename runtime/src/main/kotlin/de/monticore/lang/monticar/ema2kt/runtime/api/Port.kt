/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

import de.monticore.lang.monticar.ema2kt.runtime.PortImplementation
import de.monticore.lang.monticar.ema2kt.runtime.requireNotBlank

interface Port {
    val name: String
    var value: Any?
    fun clear()

    companion object {
        fun createMany(names: Set<String>): Set<Port> {
            return names.map { create(it) }.toSet()
        }

        fun create(name: String): Port {
            name.requireNotBlank("port name is blank")
            return PortImplementation(name)
        }
    }
}
