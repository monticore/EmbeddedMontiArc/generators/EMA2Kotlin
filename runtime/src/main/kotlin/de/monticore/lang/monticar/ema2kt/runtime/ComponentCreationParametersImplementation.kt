/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter

internal class ComponentCreationParametersImplementation(
        override val typeParameters: List<Parameter<String>> = emptyList(),
        override val parameters: List<Parameter<Any>> = emptyList()
) : ComponentCreationParameters
