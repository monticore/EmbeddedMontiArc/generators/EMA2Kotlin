/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.order

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.runtime.command.ComponentCommand

internal interface ExecutionOrderBuilder {
    fun buildExecutionOrder(meta: ComponentMetaData): List<ComponentCommand>
}
