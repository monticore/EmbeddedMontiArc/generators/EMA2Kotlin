/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

import de.monticore.lang.monticar.ema2kt.runtime.ComponentCreationParametersImplementation

interface ComponentCreationParameters {
    val typeParameters: List<Parameter<String>>
    val parameters: List<Parameter<Any>>

    companion object {
        fun create(): ComponentCreationParameters {
            return create(emptyList(), emptyList())
        }

        fun create(
                typeParameters: List<Parameter<String>>,
                parameters: List<Parameter<Any>>
        ): ComponentCreationParameters {
            return ComponentCreationParametersImplementation(
                    typeParameters,
                    parameters
            )
        }
    }
}
