/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

import de.monticore.lang.monticar.ema2kt.runtime.ParametersMetaDataImplementation

interface ParametersMetaData<out T> {
    val parameterNames: Set<String>
    val defaultValuesOfParameters: List<Parameter<T>>
    val parametersPassed2Children: List<PassedParameter>
    val literalInitializationParametersForChildren: Map<String, List<Parameter<T>>>

    companion object {
        fun <T> create(): ParametersMetaData<T> {
            return ParametersMetaDataImplementation<T>()
        }

        fun <T> create(
                parameters: Set<String>,
                defaultValuesOfParameters: List<Parameter<T>>,
                parametersPassed2Children: List<PassedParameter>,
                literalInitializationParametersForChildren: Map<String, List<Parameter<T>>>
        ): ParametersMetaData<T> {
            return ParametersMetaDataImplementation<T>(
                    parameters,
                    defaultValuesOfParameters,
                    parametersPassed2Children,
                    literalInitializationParametersForChildren
            )
        }
    }
}
