/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.command

import de.monticore.lang.monticar.ema2kt.runtime.api.Component

internal interface ComponentCommand {
    fun execute(component: Component)
}
