/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.ExecutionException

internal fun String.requireNotBlank(errorMessage: String) {
    if (isBlank()) {
        throw ExecutionException(errorMessage)
    }
}
