/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

interface ComponentMetaData {
    val componentId: String
    val inPortNames: Set<String>
    val outPortNames: Set<String>
    val typeParametersMetaData: ParametersMetaData<String>
    val parametersMetaData: ParametersMetaData<Any>
    val childName2ChildMetaData: Map<String, ComponentMetaData>
    val portConnections: List<PortConnection>
}
