/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentFactory
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentFactoryFunction
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters

internal class ComponentFactoryImplementation : ComponentFactory {
    private val factoryFunctions = mutableMapOf<String, ComponentFactoryFunction>()

    override fun registerFactoryFunction(componentId: String, f: ComponentFactoryFunction) {
        factoryFunctions.put(componentId, f)
    }

    override fun isFactoryFunctionRegistered(componentId: String): Boolean {
        return factoryFunctions.containsKey(componentId)
    }

    override fun createComponent(componentId: String, params: ComponentCreationParameters): Component? {
        return factoryFunctions[componentId]?.invoke(params)
    }
}
