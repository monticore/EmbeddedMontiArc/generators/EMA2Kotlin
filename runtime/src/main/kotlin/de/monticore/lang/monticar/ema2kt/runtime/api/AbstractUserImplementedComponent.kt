/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.api

abstract class AbstractUserImplementedComponent(
        override val inPorts: Set<Port>,
        override val outPorts: Set<Port>
) : Component {
    override val children: Map<String, Component>
        get() = emptyMap()
}
