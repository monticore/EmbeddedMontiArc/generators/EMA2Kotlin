/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.command

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.getChild

internal class ExecuteChildCommand(val childName: String) : ComponentCommand {
    override fun execute(component: Component) {
        component.getChild(childName).execute()
    }
}
