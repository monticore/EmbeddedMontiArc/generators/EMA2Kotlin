/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.runtime.order

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.runtime.api.ExecutionException
import de.monticore.lang.monticar.ema2kt.runtime.api.PortConnection
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

internal class TopologicalExecutionOrderBuilderTest {
    lateinit var target: TopologicalExecutionOrderBuilder

    @Before
    fun setup() {
        target = TopologicalExecutionOrderBuilder()
    }

    @Test(expected = ExecutionException::class)
    fun exceptionIfComponentDependenciesFormLoop() {
        val metaE1 = mockMetaData(
                "test.component.E1",
                setOf("in1", "in2", "in3"),
                setOf("out1", "out2", "out3")
        )
        val metaE2 = mockMetaData(
                "test.component.E2",
                setOf("in1"),
                setOf("out1")
        )

        val metaE3 = mockMetaData(
                "test.component.E3",
                setOf("in1", "in2"),
                setOf("out1", "out2")
        )
        val metaC1 = mockMetaData(
                "test.component.C1",
                setOf("c_in1", "c_in2"),
                setOf("c_out1", "c_out2")
        )
        Mockito.doReturn(mapOf(
                "e1" to metaE1,
                "e2" to metaE2,
                "e3" to metaE3
        )).`when`(metaC1).childName2ChildMetaData
        Mockito.doReturn(listOf(
                PortConnection.self2Child("c_in1", "e1", "in1"),
                PortConnection.child2Child("e1", "out1", "e2", "in1"),
                PortConnection.child2Child("e2", "out1", "e3", "in1"),
                PortConnection.child2Child("e3", "out2", "e1", "in2"),
                PortConnection.child2Self("e3", "out1", "c_out1")
        )).`when`(metaC1).portConnections
        target.buildExecutionOrder(metaC1)
    }

    private fun mockMetaData(
            componentId: String,
            inPortNames: Set<String>,
            outPortNames: Set<String>
    ): ComponentMetaData {
        val mock = Mockito.mock(ComponentMetaData::class.java)
        Mockito.doReturn(componentId).`when`(mock).componentId
        Mockito.doReturn(inPortNames).`when`(mock).inPortNames
        Mockito.doReturn(outPortNames).`when`(mock).outPortNames
        return mock
    }
}
