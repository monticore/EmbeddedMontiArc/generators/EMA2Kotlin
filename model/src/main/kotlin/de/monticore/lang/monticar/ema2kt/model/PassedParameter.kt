/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class PassedParameter(
        val sourceParameterName: String,
        val targetChildName: String,
        val targetParameterName: String
)
