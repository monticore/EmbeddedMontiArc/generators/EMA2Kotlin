/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class StructField(
        val name: String,
        val type: String,
        val initializer: String
)
