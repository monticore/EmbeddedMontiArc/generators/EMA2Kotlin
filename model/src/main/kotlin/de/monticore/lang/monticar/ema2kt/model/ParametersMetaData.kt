/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

class ParametersMetaData<T>(
        val parameterNames: MutableSet<String> = mutableSetOf(),
        val parameterName2Type: MutableMap<String, String> = mutableMapOf(),
        val defaultValuesOfParameters: MutableList<Parameter<T>> = mutableListOf(),
        val parametersPassed2Children: MutableList<PassedParameter> = mutableListOf(),
        val literalInitializationParametersForChildren: MutableMap<String, List<Parameter<T>>> = mutableMapOf()
)
