/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class ChildOut2SelfOutPortConnection(
        val sourceChild: String,
        val sourceOutPort: Port,
        val targetOutPort: Port
) : IPortConnection
