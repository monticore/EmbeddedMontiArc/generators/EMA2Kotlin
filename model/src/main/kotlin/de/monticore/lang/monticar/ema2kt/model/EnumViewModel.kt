/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class EnumViewModel(
        override val fullyQualifiedNameParts: List<String>,
        val constants: Set<String>
) : IEntity
