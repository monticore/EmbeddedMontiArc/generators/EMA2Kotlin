/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

class ComponentMetaData(
        override val fullyQualifiedNameParts: List<String>,
        val children: Map<String, ComponentMetaData>
) : IEntity {
    lateinit var component: Component
}
