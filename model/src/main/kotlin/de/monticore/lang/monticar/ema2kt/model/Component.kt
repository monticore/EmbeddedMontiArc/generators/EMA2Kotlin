/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

class Component(
        override val fullyQualifiedNameParts: List<String>,
        val inPorts: Set<Port>,
        val outPorts: Set<Port>,
        val typeParametersMetaData: ParametersMetaData<String> = ParametersMetaData(),
        val parametersMetaData: ParametersMetaData<Any> = ParametersMetaData(),
        val children: MutableMap<String, Component> = mutableMapOf(),
        val portConnections: MutableList<IPortConnection> = mutableListOf()
) : IEntity {
    lateinit var metaData: ComponentMetaData
}

fun Component.getChild(childName: String): Component? {
    return children[childName]
}

fun Component.getInPort(portName: String): Port? {
    return inPorts.getPort(portName)
}

fun Component.getOutPort(portName: String): Port? {
    return outPorts.getPort(portName)
}

private fun Iterable<Port>.getPort(portName: String): Port? {
    return find { it.name == portName }
}
