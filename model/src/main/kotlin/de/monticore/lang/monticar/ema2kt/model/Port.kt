/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class Port(
        val name: String,
        val type: String
)
