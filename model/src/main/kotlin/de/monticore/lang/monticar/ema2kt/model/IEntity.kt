/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

interface IEntity {
    val fullyQualifiedNameParts: List<String>
}

val IEntity.fullyQualifiedName: String
    get() {
        return fullyQualifiedNameParts.toFullyQualifiedName()
    }

val IEntity.packageName: String
    get() {
        return packageNameParts.toFullyQualifiedName()
    }

val IEntity.packageNameParts: List<String>
    get() {
        return fullyQualifiedNameParts.dropLast(1)
    }

val IEntity.name: String
    get() {
        return fullyQualifiedNameParts.last()
    }

val FULLY_QUALIFIED_NAME_SEPARATOR = "."

fun Iterable<String>.toFullyQualifiedName(): String {
    return joinToString(FULLY_QUALIFIED_NAME_SEPARATOR)
}

fun String.splitIntoParts(): List<String> {
    return split(FULLY_QUALIFIED_NAME_SEPARATOR)
}
