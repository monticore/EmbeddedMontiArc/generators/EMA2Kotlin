/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

class Parameter<out T>(
        val name: String,
        val value: T?
)
