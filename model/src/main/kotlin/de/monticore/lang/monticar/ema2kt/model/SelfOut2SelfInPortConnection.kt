/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class SelfOut2SelfInPortConnection(
        val sourceOutPort: Port,
        val targetInPort: Port
) : IPortConnection
