/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class Struct(
        override val fullyQualifiedNameParts: List<String>,
        val fields: Set<StructField>
) : IEntity
