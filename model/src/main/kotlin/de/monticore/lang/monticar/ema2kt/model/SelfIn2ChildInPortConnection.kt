/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class SelfIn2ChildInPortConnection(
        val sourceInPort: Port,
        val targetChild: String,
        val targetInPort: Port
) : IPortConnection
