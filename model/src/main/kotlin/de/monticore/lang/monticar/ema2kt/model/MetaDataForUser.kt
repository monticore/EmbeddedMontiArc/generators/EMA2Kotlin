/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

class MetaDataForUser(
        override val fullyQualifiedNameParts: List<String>,
        val componentsMeta: List<ComponentMetaData>
) : IEntity
