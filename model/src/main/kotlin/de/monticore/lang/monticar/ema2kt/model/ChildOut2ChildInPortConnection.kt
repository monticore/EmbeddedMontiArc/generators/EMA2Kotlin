/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

data class ChildOut2ChildInPortConnection(
        val sourceChild: String,
        val sourceOutPort: Port,
        val targetChild: String,
        val targetInPort: Port
) : IPortConnection
