/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model

class DomainModel(
        val enums: List<EnumViewModel>,
        val structures: List<Struct>,
        val components: List<Component>,
        val componentMetaData: List<ComponentMetaData>,
        val metaDataForUser: MetaDataForUser
)
