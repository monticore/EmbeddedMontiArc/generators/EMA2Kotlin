/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.meta.modelling.project1.component.test.sub2.MetaDataForFuncWithSelfFeedbackLoop
import org.junit.Assert
import org.junit.Test

class TestFuncWithSelfFeedbackLoop : TestBase(MetaDataForFuncWithSelfFeedbackLoop) {

    @Test
    fun test() {
        val inputs1 = listOf(1.0, 2.0).toCanonicalInputs()
        val outputs1 = executeSystem(inputs1)
        Assert.assertNull(outputs1["out1"])
        Assert.assertNotNull(outputs1["out2"])
        Assert.assertEquals(3.0, outputs1["out2"] as Double, NUMERIC_PRECISION)
        val inputs2 = listOf(3.0, 4.0).toCanonicalInputs()
        val outputs2 = executeSystem(inputs2)
        Assert.assertNotNull(outputs2["out1"])
        Assert.assertEquals(10.0, outputs2["out1"] as Double, NUMERIC_PRECISION)
        Assert.assertNotNull(outputs2["out2"])
        Assert.assertEquals(7.0, outputs2["out2"] as Double, NUMERIC_PRECISION)
    }
}
