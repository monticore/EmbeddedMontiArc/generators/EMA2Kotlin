/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentBuilder
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentSystem
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.monticore.lang.monticar.ema2kt.runtime.api.toComponentSystem
import modelling.project1.meta.modelling.project1.component.test.sub3.ConfigurationParametersOfBlock1
import modelling.project1.meta.modelling.project1.component.test.sub3.MetaDataForBlock1
import modelling.project1.meta.modelling.project1.component.test.sub3.PortsOfBlock1
import org.junit.Assert
import org.junit.Test

class TestBlock1 {

    val PRECISION = 1e-5

    val builder = MyComponentBuilder(ComponentBuilder.create())
    lateinit var targetAsComponent: Component
    lateinit var targetAsSystem: ComponentSystem

    @Test
    fun testWithPrecision3() {
        setup(3)
        val result1 = run(null, 1.0)
        Assert.assertEquals(-1.0, result1 as Double, PRECISION)
        val result2 = run(null, 10.0)
        Assert.assertEquals(-10.0, result2 as Double, PRECISION)
        val result3 = run(2.759, 4.831)
        Assert.assertEquals(13.329, result3 as Double, PRECISION)
    }

    @Test
    fun testWithPrecision2() {
        setup(2)
        val result1 = run(2.2, 3.2)
        Assert.assertEquals(7.04, result1 as Double, PRECISION)
        val result2 = run(null, 1e-5)
        Assert.assertEquals(0.0, result2 as Double, PRECISION)
        val result3 = run(1e+5, 1e-5)
        Assert.assertEquals(1.0, result3 as Double, PRECISION)
        val result4 = run(null, 0.999)
        Assert.assertEquals(-1.0, result4 as Double, PRECISION)
        val result5 = run(null, -0.99)
        Assert.assertEquals(0.99, result5 as Double, PRECISION)

    }

    @Test
    fun testWithNullInputs() {
        setup(5)
        val result1 = run(null, null)
        Assert.assertNull(result1)
        val result2 = run(null, 1.27)
        Assert.assertEquals(-1.27, result2 as Double, PRECISION)
        val result3 = run(null, null)
        Assert.assertNull(result3)
        val result4 = run(0.1234567, 0.7654321)
        Assert.assertEquals(0.09450, result4 as Double, PRECISION)
        val result5 = run(1.0, null)
        Assert.assertNull(result5)
    }

    private fun run(in1: Double?, in2: Double?): Double? {
        val outputs = targetAsSystem.execute(mapOf(
                PortsOfBlock1.inPort.in1 to in1,
                PortsOfBlock1.inPort.in2 to in2
        ))
        return outputs[PortsOfBlock1.outPort.out1] as? Double
    }

    private fun setup(precision: Long) {
        targetAsComponent = builder.buildComponent(
                MetaDataForBlock1,
                ComponentCreationParameters.create(
                        emptyList(),
                        listOf(Parameter.create(ConfigurationParametersOfBlock1.precision, precision))
                )
        )
        targetAsSystem = targetAsComponent.toComponentSystem()
    }
}
