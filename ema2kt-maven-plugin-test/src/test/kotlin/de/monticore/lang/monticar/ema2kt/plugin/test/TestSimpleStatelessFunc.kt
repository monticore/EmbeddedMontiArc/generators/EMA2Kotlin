/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.meta.modelling.project1.component.test.sub2.MetaDataForSimpleStatelessFunc
import org.junit.Assert
import org.junit.Test

class TestSimpleStatelessFunc : TestBase(MetaDataForSimpleStatelessFunc) {

    @Test
    fun test() {
        val values = listOf(
                null,
                -1000.0,
                -100.0,
                -10.0,
                -3.78,
                -1.0,
                -0.0001,
                +0.0,
                +0.000901,
                +0.33,
                +1.0,
                +1.78,
                +5.44,
                +25.71,
                +100.0,
                +1000.0
        )
        val inputsForFunc1 = mutableListOf<List<Double?>>()
        for (in1 in values)
            for (in2 in values)
                for (in3 in values)
                    for (in4 in values)
                        for (in5 in values) {
                            inputsForFunc1.add(listOf(
                                    in1,
                                    in2,
                                    in3,
                                    in4,
                                    in5
                            ))
                        }
        inputsForFunc1.forEach { testOn(it) }
    }

    private fun testOn(inputs: List<Double?>) {
        val expectedResult = simpleStatelessFunc(inputs)
        val outputs = executeSystem(inputs.toCanonicalInputs())
        val result = outputs["out1"] as? Double
        if (expectedResult == null) {
            Assert.assertNull("expected null but got $result", result)
        } else {
            Assert.assertNotNull("expected $expectedResult but got null", result)
            Assert.assertEquals("expected $expectedResult but got $result", expectedResult, result as Double, NUMERIC_PRECISION)
        }
    }
}

private fun simpleStatelessFunc(inputs: List<Double?>): Double? {
    val in1 = inputs[0]
    val in2 = inputs[1]
    val in3 = inputs[2]
    val in4 = inputs[3]
    val in5 = inputs[4]
    return add(
            mult(
                    mult(in1, in2),
                    add(in3, in4)
            ),
            in5
    )
}

private fun add(x: Double?, y: Double?): Double? {
    return if (x != null && y != null) {
        x + y
    } else {
        null
    }
}

private fun mult(x: Double?, y: Double?): Double? {
    return if (x != null && y != null) {
        x * y
    } else {
        null
    }
}
