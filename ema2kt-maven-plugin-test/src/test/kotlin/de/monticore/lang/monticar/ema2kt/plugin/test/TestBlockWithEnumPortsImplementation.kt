/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import de.monticore.lang.monticar.ema2kt.runtime.api.toComponentSystem
import modelling.project1.enums.test.sub5.Foo
import modelling.project1.meta.modelling.project1.component.test.sub5.PortsOfBlockWithEnumPorts
import org.junit.Assert
import org.junit.Test

class TestBlockWithEnumPortsImplementation {

    @Test
    fun test() {
        val b = BlockWithEnumPortsImplementation().toComponentSystem()
        val data = listOf(
                Pair(Foo.FOO, Foo.BAR),
                Pair(Foo.BAR, Foo.BAZ),
                Pair(Foo.BAZ, Foo.FOO)
        )
        data.forEach { (input, expectedOutput) ->
            val outputs = b.execute(mapOf(
                    PortsOfBlockWithEnumPorts.inPort.in1 to input
            ))
            Assert.assertEquals(1, outputs.size)
            Assert.assertEquals(expectedOutput, outputs.get(PortsOfBlockWithEnumPorts.outPort.out1))
        }
    }
}
