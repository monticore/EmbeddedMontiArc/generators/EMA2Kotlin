/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentBuilder
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentSystem
import de.monticore.lang.monticar.ema2kt.runtime.api.toComponentSystem
import org.junit.Before

internal val NUMERIC_PRECISION = 1e-5

abstract class TestBase(
        protected val meta: ComponentMetaData,
        protected val params: ComponentCreationParameters = ComponentCreationParameters.create()
) {
    protected val builder = MyComponentBuilder(ComponentBuilder.create())
    protected lateinit var targetAsComponent: Component
    protected lateinit var targetAsSystem: ComponentSystem

    @Before
    open fun setup() {
        targetAsComponent = builder.buildComponent(meta, params)
        targetAsSystem = targetAsComponent.toComponentSystem()
    }

    protected fun executeSystem(inputs: Map<String, Any?>): Map<String, Any?> {
        return targetAsSystem.execute(inputs)
    }
}

internal fun Iterable<Any?>.toCanonicalInputs(): Map<String, Any?> {
    return mapIndexed { index, obj -> "in${index + 1}" to obj }.toMap()
}
