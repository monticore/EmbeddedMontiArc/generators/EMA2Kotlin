/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.component.test.sub2.Mult

class MultImplementation : Mult() {
    override fun execute() {
        if (in1 != null && in2 != null) {
            setOut1((in1 ?: 0.0) * (in2 ?: 0.0))
        }
    }
}
