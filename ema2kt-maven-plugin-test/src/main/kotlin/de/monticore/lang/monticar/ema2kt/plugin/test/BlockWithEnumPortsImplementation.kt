/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.component.test.sub5.BlockWithEnumPorts
import modelling.project1.enums.test.sub5.Foo

class BlockWithEnumPortsImplementation : BlockWithEnumPorts() {

    override fun execute() {
        val input1 = in1 ?: return
        val output1 = when (input1) {
            Foo.FOO -> Foo.BAR
            Foo.BAR -> Foo.BAZ
            Foo.BAZ -> Foo.FOO
        }
        setOut1(output1)
    }
}
