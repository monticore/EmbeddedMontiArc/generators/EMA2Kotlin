/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.component.test.sub3.PID

class PIDImplementation(
        paramP: Double,
        paramI: Double,
        paramD: Double,
        min: Double,
        max: Double
) : PID(paramP, paramI, paramD, min, max) {

    private var previousError: Double? = null
    private var accumulatedError = 0.0

    override fun execute() {
        if (error != null) {
            val diff = if (previousError != null) {
                error!! - previousError!!
            } else {
                0.0
            }
            accumulatedError += error!!
            val control = paramD * diff + paramI * accumulatedError + paramD * diff / 1.0
            setControl(when {
                control < min -> min
                control > max -> max
                else -> control
            })
            previousError = error
        }
    }
}
