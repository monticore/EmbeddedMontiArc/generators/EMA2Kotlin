/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.component.test.sub3.Transformer

class ToStringTransformerImplementation : Transformer<Any?, String>() {
    override fun execute() {
        setOutput(input.toString())
    }
}
