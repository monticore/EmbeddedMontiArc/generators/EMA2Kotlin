/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentBuilder
import modelling.project1.meta.ToBeImplemented
import modelling.project1.meta.modelling.project1.component.test.sub3.ConfigurationParametersOfMemoryCell
import modelling.project1.meta.modelling.project1.component.test.sub3.ConfigurationParametersOfPID
import modelling.project1.meta.modelling.project1.component.test.sub3.ConfigurationParametersOfRound
import modelling.project1.meta.modelling.project1.component.test.sub3.TypeParametersOfTransformer

class MyComponentBuilder(builder: ComponentBuilder) : ComponentBuilder by builder {
    init {
        val factory = builder.factory
        factory.registerFactoryFunction(ToBeImplemented.idOfAdd, { _ -> AddImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfMult, { _ -> MultImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfMemoryCell, {
            val defaultValue = it.parameters.single {
                it.name == ConfigurationParametersOfMemoryCell.defaultValue
            }.value!!
            MemoryCellImplementation<Any>(defaultValue)
        })
        factory.registerFactoryFunction(ToBeImplemented.idOfPID, {
            val p = it.parameters.single {
                it.name == ConfigurationParametersOfPID.paramP
            }.value as Double
            val i = it.parameters.single {
                it.name == ConfigurationParametersOfPID.paramI
            }.value as Double
            val d = it.parameters.single {
                it.name == ConfigurationParametersOfPID.paramD
            }.value as Double
            val min = it.parameters.single {
                it.name == ConfigurationParametersOfPID.min
            }.value as Double
            val max = it.parameters.single {
                it.name == ConfigurationParametersOfPID.max
            }.value as Double
            PIDImplementation(p, i, d, min, max)
        })
        factory.registerFactoryFunction(ToBeImplemented.idOfTransformer, {
            val t1 = it.typeParameters.single {
                it.name == TypeParametersOfTransformer.T1
            }.value!!
            val t2 = it.typeParameters.single {
                it.name == TypeParametersOfTransformer.T2
            }.value!!
            when {
                t2.toLowerCase().endsWith("string") -> {
                    ToStringTransformerImplementation()
                }
                else -> throw UnsupportedOperationException(
                        "no transformer from $t1 to $t2"
                )
            }
        })
        factory.registerFactoryFunction(ToBeImplemented.idOfRound, {
            val precision = it.parameters.single {
                it.name == ConfigurationParametersOfRound.precision
            }.value as Long
            RoundImplementation(precision)
        })
    }
}
