/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.component.test.sub3.Round
import java.math.BigDecimal
import java.math.RoundingMode

class RoundImplementation(
        precision: Long
) : Round(precision) {
    override fun execute() {
        if (input != null) {
            val rounded = BigDecimal.valueOf(input!!).setScale(precision.toInt(), RoundingMode.HALF_UP)
            setOutput(rounded.toDouble())
        }
    }
}
