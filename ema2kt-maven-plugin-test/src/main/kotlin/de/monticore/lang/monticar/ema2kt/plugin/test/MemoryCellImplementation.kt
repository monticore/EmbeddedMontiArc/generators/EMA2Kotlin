/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.component.test.sub3.MemoryCell

class MemoryCellImplementation<T>(
        defaultValue: T
) : MemoryCell<T>(defaultValue) {

    override fun execute() {
        setOutput(when {
            input != null -> input!!
            else -> defaultValue
        })
    }
}
