/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin.test

import modelling.project1.component.test.sub4.BlockWithArrayInput

class BlockWithArrayInputImplementation : BlockWithArrayInput() {

    override fun execute() {
        val inputData = in1 ?: emptyList()
        setOut1(inputData.size % 2 == 0)
    }
}
