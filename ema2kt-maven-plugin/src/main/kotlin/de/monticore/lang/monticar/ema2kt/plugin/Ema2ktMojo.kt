/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.plugin

import de.monticore.lang.monticar.ema2kt.generator.Generator
import de.monticore.lang.monticar.ema2kt.generator.GeneratorException
import de.monticore.lang.monticar.ema2kt.generator.GeneratorSettings
import de.monticore.lang.monticar.ema2kt.model.FULLY_QUALIFIED_NAME_SEPARATOR
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import java.io.File
import java.nio.file.Path

private val DEFAULT_EMA_SOURCES_PATH = "src/main/ema"

@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
class Ema2ktMojo : AbstractMojo() {
    @Parameter(defaultValue = "\${project.basedir}", required = true, readonly = true)
    var baseDir: File? = null

    @Parameter(required = false, readonly = true)
    var sourceDirs: List<File>? = null

    @Parameter(defaultValue = "\${project.build.directory}/src-gen/kotlin", required = true, readonly = true)
    var outputDir: File? = null

    @Parameter(required = false, readonly = true)
    var packagePrefix: String? = null

    private val baseDirPath: Path
        get() = baseDir!!.toPath()

    private val sourceDirPaths: List<Path>
        get() {
            val providedSourceDirs = sourceDirs ?: emptyList()
            return if (providedSourceDirs.isEmpty()) {
                listOf(baseDirPath.resolve(DEFAULT_EMA_SOURCES_PATH))
            } else {
                providedSourceDirs.map(File::toPath)
            }
        }

    private val packagePrefixParts: List<String>
        get() {
            return packagePrefix?.split(FULLY_QUALIFIED_NAME_SEPARATOR) ?: emptyList()
        }

    @Throws(MojoExecutionException::class)
    override fun execute() {
        try {
            val settings = GeneratorSettings(packagePrefixParts)
            val generator = Generator.create(settings)
            generator.generate(sourceDirPaths, outputDir!!.toPath())
        } catch (error: GeneratorException) {
            throw MojoFailureException(
                    "error during code generation: ${error.message}",
                    error
            )
        } catch (error: Exception) {
            throw MojoExecutionException(
                    "unexpected error occurred: ${error.message}",
                    error
            )
        }
    }
}
