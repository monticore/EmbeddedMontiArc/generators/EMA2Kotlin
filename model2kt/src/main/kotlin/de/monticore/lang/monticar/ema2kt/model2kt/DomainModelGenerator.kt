/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model2kt

import de.monticore.lang.monticar.ema2kt.model.DomainModel
import java.nio.file.Path

interface DomainModelGenerator {
    fun generate(domainModel: DomainModel, outputPath: Path)

    companion object {
        fun create(): DomainModelGenerator {
            return DomainModelGeneratorImplementation()
        }
    }
}
