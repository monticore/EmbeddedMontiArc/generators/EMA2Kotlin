/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model2kt.template

import de.monticore.lang.monticar.ema2kt.model.ChildOut2ChildInPortConnection
import de.monticore.lang.monticar.ema2kt.model.ChildOut2SelfOutPortConnection
import de.monticore.lang.monticar.ema2kt.model.IEntity
import de.monticore.lang.monticar.ema2kt.model.Parameter
import de.monticore.lang.monticar.ema2kt.model.SelfIn2ChildInPortConnection
import de.monticore.lang.monticar.ema2kt.model.SelfOut2SelfInPortConnection
import de.monticore.lang.monticar.ema2kt.model.fullyQualifiedName
import de.monticore.lang.monticar.ema2kt.model.name
import de.monticore.lang.monticar.ema2kt.model.packageName

internal object Helper {
    fun getPackageName(entity: IEntity): String {
        return entity.packageName
    }

    fun getName(entity: IEntity): String {
        return entity.name
    }

    fun getFullyQualifiedName(entity: IEntity): String {
        return entity.fullyQualifiedName
    }

    fun isSelf2Child(portConnection: Any?): Boolean {
        return portConnection is SelfIn2ChildInPortConnection
    }

    fun isChild2Child(portConnection: Any?): Boolean {
        return portConnection is ChildOut2ChildInPortConnection
    }

    fun isChild2Self(portConnection: Any?): Boolean {
        return portConnection is ChildOut2SelfOutPortConnection
    }

    fun isSelf2Self(portConnection: Any?): Boolean {
        return portConnection is SelfOut2SelfInPortConnection
    }

    fun <T> getRepresentationOfDefaultValue(
            defaultValues: Iterable<Parameter<T>>,
            parameterName: String
    ): String {
        val parameter = defaultValues.find { it.name == parameterName } ?: return ""
        return representAsLiteral(parameter.value)
    }

    fun representAsLiteral(obj: Any?): String {
        when (obj) {
            is String -> {
                val s = obj.replace("\"", "\\\"")
                return "\"$s\""
            }
        }
        return obj.toString()
    }

    fun getDataForTemplate(viewModel: IEntity): MutableMap<String, Any> {
        return mutableMapOf(
                "viewModel" to viewModel,
                "helper" to Helper
        )
    }
}
