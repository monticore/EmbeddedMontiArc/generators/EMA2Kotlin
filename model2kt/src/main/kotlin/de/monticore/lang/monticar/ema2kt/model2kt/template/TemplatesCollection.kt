/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model2kt.template

import freemarker.template.Configuration
import freemarker.template.Template
import freemarker.template.TemplateExceptionHandler

internal interface TemplatesCollection {
    val templateForEnum: Template
    val templateForStructure: Template
    val templateForComponent: Template
    val templateForComponentMetaData: Template
    val templateForUserMetaData: Template

    companion object {
        fun create(): TemplatesCollection {
            val configuration = Configuration(Configuration.VERSION_2_3_23)
            configuration.setClassForTemplateLoading(javaClass, "/template")
            configuration.defaultEncoding = "UTF-8"
            configuration.templateExceptionHandler = TemplateExceptionHandler.DEBUG_HANDLER
            configuration.logTemplateExceptions = false
            return TemplatesCollectionImplementation(configuration)
        }
    }
}

private class TemplatesCollectionImplementation(
        configuration: Configuration
) : TemplatesCollection {
    override val templateForEnum: Template = configuration.getTemplate("/enum/Enum.ftl")
    override val templateForStructure: Template = configuration.getTemplate("/structure/Structure.ftl")
    override val templateForComponent: Template = configuration.getTemplate("/component/Component.ftl")
    override val templateForComponentMetaData: Template = configuration.getTemplate("/component/ComponentMetaData.ftl")
    override val templateForUserMetaData: Template = configuration.getTemplate("/component/MetaDataForUser.ftl")
}
