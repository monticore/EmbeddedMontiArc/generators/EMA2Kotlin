/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model2kt

import de.monticore.lang.monticar.ema2kt.model.DomainModel
import de.monticore.lang.monticar.ema2kt.model.IEntity
import de.monticore.lang.monticar.ema2kt.model.name
import de.monticore.lang.monticar.ema2kt.model.packageNameParts
import de.monticore.lang.monticar.ema2kt.model2kt.template.Helper
import de.monticore.lang.monticar.ema2kt.model2kt.template.TemplatesCollection
import freemarker.template.Template
import java.io.File
import java.io.FileWriter
import java.nio.file.Path


internal class DomainModelGeneratorImplementation : DomainModelGenerator {
    private val templates = TemplatesCollection.create()

    override fun generate(domainModel: DomainModel, outputPath: Path) {
        domainModel.enums.forEach {
            generate(templates.templateForEnum, it, outputPath)
        }
        domainModel.structures.forEach {
            generate(templates.templateForStructure, it, outputPath)
        }
        domainModel.componentMetaData.forEach {
            generate(templates.templateForComponentMetaData, it, outputPath)
        }
        generate(templates.templateForUserMetaData, domainModel.metaDataForUser, outputPath)
        domainModel.components.filter { it.children.isEmpty() }.forEach {
            generate(templates.templateForComponent, it, outputPath)
        }
    }

    private fun generate(template: Template, viewModel: IEntity, outputPath: Path) {
        val dataForTemplate = Helper.getDataForTemplate(viewModel)
        val outputFile = createOutputFileFor(viewModel, outputPath)
        var fileWriter: FileWriter? = null
        try {
            fileWriter = FileWriter(outputFile)
            template.process(dataForTemplate, fileWriter)
        } finally {
            fileWriter?.close()
        }
    }

    private fun createOutputFileFor(entity: IEntity, outputPath: Path): File {
        val outputFolderPath = entity.packageNameParts.fold(
                outputPath,
                { currentPath, nextPart -> currentPath.resolve(nextPart) }
        )
        val outputFolder = outputFolderPath.toFile()
        if (!outputFolder.exists()) {
            outputFolder.mkdirs()
        }
        val outputFile = outputFolderPath.resolve("${entity.name}.kt").toFile()
        outputFile.createNewFile()
        return outputFile
    }
}
