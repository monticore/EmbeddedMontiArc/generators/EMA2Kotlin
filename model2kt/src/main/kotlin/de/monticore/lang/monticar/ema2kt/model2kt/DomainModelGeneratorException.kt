/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model2kt

class DomainModelGeneratorException(message: String) : Exception(message)
