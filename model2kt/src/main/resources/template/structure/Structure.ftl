<#-- (c) https://github.com/MontiCore/monticore -->
<#include "/Package.ftl">

data class ${helper.getName(viewModel)} (
<#list viewModel.fields as field>
  var ${field.name}: ${field.type}<#if field.initializer?has_content> = ${field.initializer}</#if><#sep>,</#sep>
</#list>
)
