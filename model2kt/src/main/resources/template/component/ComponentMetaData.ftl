<#-- (c) https://github.com/MontiCore/monticore -->
<#include "/Package.ftl">

import de.monticore.lang.monticar.ema2kt.runtime.api.AbstractComponentMetaData
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.monticore.lang.monticar.ema2kt.runtime.api.ParametersMetaData
import de.monticore.lang.monticar.ema2kt.runtime.api.PassedParameter
import de.monticore.lang.monticar.ema2kt.runtime.api.PortConnection

<#assign componentName = helper.getName(viewModel.component)><#t>
<#assign portsOfComponent = "PortsOf${componentName}"><#t>
<#assign typeParametersOfComponent = "TypeParametersOf${componentName}"><#t>
<#assign allTypeParametersProperty = "all"><#t>
<#assign configurationParametersOfComponent = "ConfigurationParametersOf${componentName}"><#t>
<#assign allConfigurationParametersProperty = "all"><#t>

object ${portsOfComponent} {
    object inPort {
        <@renderPortsNames ports=viewModel.component.inPorts />
    }

    val allInPorts = setOf(
        <@renderAllPorts prefix="inPort." ports=viewModel.component.inPorts />
    )

    object outPort {
        <@renderPortsNames ports=viewModel.component.outPorts />
    }

    val allOutPorts = setOf(
        <@renderAllPorts prefix="outPort." ports=viewModel.component.outPorts />
    )
}

<#macro renderPortsNames ports>
<#list ports as p>
val ${p.name} = "${p.name}"
</#list>
</#macro>

<#macro renderAllPorts prefix ports>
<#list ports as p>
${prefix}${p.name}<#sep>,</#sep>
</#list>
</#macro>



<@renderParametersObject name = typeParametersOfComponent
    parameters = viewModel.component.typeParametersMetaData.parameterNames
    allParametersPropertyName = allTypeParametersProperty />

<@renderParametersObject name = configurationParametersOfComponent
    parameters = viewModel.component.parametersMetaData.parameterNames
    allParametersPropertyName = allConfigurationParametersProperty />

object ${helper.getName(viewModel)} : AbstractComponentMetaData(
        componentId = "${helper.getFullyQualifiedName(viewModel.component)}"
        , inPortNames = ${portsOfComponent}.allInPorts
        , outPortNames = ${portsOfComponent}.allOutPorts
        , typeParametersMetaData = <@renderParametersMetaData meta = viewModel.component.typeParametersMetaData
                                       parametersObject = typeParametersOfComponent
                                       allParametersPropertyName = allTypeParametersProperty />
        , parametersMetaData = <@renderParametersMetaData meta = viewModel.component.parametersMetaData
                                   parametersObject = configurationParametersOfComponent
                                   allParametersPropertyName = allConfigurationParametersProperty />
        <#if viewModel.children?size gt 0>
        , childName2ChildMetaData = mapOf(
        <#list viewModel.children?keys as childName>
                "${childName}" to ${helper.getFullyQualifiedName(viewModel.children[childName])}<#sep>,</#sep>
        </#list>
        )
        , portConnections = listOf(
        <#list viewModel.component.portConnections as pc>
                <#if helper.isSelf2Child(pc)>PortConnection.self2Child("${pc.sourceInPort.name}", "${pc.targetChild}", "${pc.targetInPort.name}")
                <#elseif helper.isChild2Child(pc)>PortConnection.child2Child("${pc.sourceChild}", "${pc.sourceOutPort.name}", "${pc.targetChild}", "${pc.targetInPort.name}")
                <#elseif helper.isChild2Self(pc)>PortConnection.child2Self("${pc.sourceChild}", "${pc.sourceOutPort.name}", "${pc.targetOutPort.name}")
                <#elseif helper.isSelf2Self(pc)>PortConnection.selfOut2SelfIn("${pc.sourceOutPort.name}", "${pc.targetInPort.name}")
                </#if><#sep>,</#sep>
        </#list>
        )
        </#if>
)

<#macro renderParametersObject name parameters allParametersPropertyName>
object ${name} {
    <@renderParametersProperties parameters = parameters />
    val ${allParametersPropertyName} = setOf<String>(
        <@renderAllParametersProperty parameters = parameters />
    )
}
</#macro>

<#macro renderParametersProperties parameters>
<#list parameters as name>
    val ${name} = "${name}"
</#list>
</#macro>

<#macro renderAllParametersProperty parameters>
<#list parameters as name>
    ${name}<#sep>,</#sep>
</#list>
</#macro>

<#macro renderParametersMetaData meta parametersObject allParametersPropertyName>
ParametersMetaData.create(
      parameters = ${parametersObject}.${allParametersPropertyName}
      , defaultValuesOfParameters = listOf(<@renderParameters parameters = meta.defaultValuesOfParameters />)
      , parametersPassed2Children = listOf(<@renderPassedParameters parameters = meta.parametersPassed2Children parametersObject = parametersObject />)
      , literalInitializationParametersForChildren = mapOf(
      <#list meta.literalInitializationParametersForChildren?keys as childName>
              "${childName}" to listOf(<@renderParameters parameters = meta.literalInitializationParametersForChildren[childName] />)<#sep>,</#sep>
      </#list>
      )
)
</#macro>

<#macro renderParameters parameters>
<#list parameters as p>
    Parameter.create("${p.name}", ${helper.representAsLiteral(p.value)})<#sep>,</#sep>
</#list>
</#macro>

<#macro renderPassedParameters parameters parametersObject>
<#list parameters as p>
    PassedParameter.create(${parametersObject}.${p.sourceParameterName}, "${p.targetChildName}", "${p.targetParameterName}")<#sep>,</#sep>
</#list>
</#macro>
