<#-- (c) https://github.com/MontiCore/monticore -->
<#include "/Package.ftl">

object ${helper.getName(viewModel)} {
        <#list viewModel.componentsMeta as meta>
        val metaFor${helper.getName(meta.component)} = ${helper.getFullyQualifiedName(meta)}
        val idOf${helper.getName(meta.component)} = metaFor${helper.getName(meta.component)}.componentId

        </#list>
        val allMetaData = listOf(
        <#list viewModel.componentsMeta as meta>
            metaFor${helper.getName(meta.component)}<#sep>,</#sep>
        </#list>
        )

        val allIds = setOf(
        <#list viewModel.componentsMeta as meta>
            idOf${helper.getName(meta.component)}<#sep>,</#sep>
        </#list>
        )
}
