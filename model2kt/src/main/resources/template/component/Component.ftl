<#-- (c) https://github.com/MontiCore/monticore -->
<#include "/Package.ftl">

import de.monticore.lang.monticar.ema2kt.runtime.api.AbstractUserImplementedComponent
import de.monticore.lang.monticar.ema2kt.runtime.api.Port
import de.monticore.lang.monticar.ema2kt.runtime.api.getInPort
import de.monticore.lang.monticar.ema2kt.runtime.api.getOutPort

abstract class ${helper.getName(viewModel)}<#t>
<#if viewModel.typeParametersMetaData.parameterNames?size gt 0><#t>
<<#t>
<#list viewModel.typeParametersMetaData.parameterNames as name><#t>
    ${name}<#sep>,</#sep><#t>
</#list><#t>
><#t>
</#if>
<#if viewModel.parametersMetaData.parameterName2Type?size gt 0><#t>
(
<#list viewModel.parametersMetaData.parameterName2Type?keys as name>
    protected open val ${name} : ${viewModel.parametersMetaData.parameterName2Type[name]}<#t>
    <#assign defaultValue = helper.getRepresentationOfDefaultValue(viewModel.parametersMetaData.defaultValuesOfParameters, name)><#t>
    <#if defaultValue?has_content> = ${defaultValue}</#if><#t>
    <#sep>,</#sep><#lt>
</#list>
)<#t>
</#if>
: AbstractUserImplementedComponent(
        inPorts = Port.createMany(${helper.getFullyQualifiedName(viewModel.metaData)}.inPortNames),
        outPorts = Port.createMany(${helper.getFullyQualifiedName(viewModel.metaData)}.outPortNames)
) {
    <#list viewModel.inPorts as p>
    protected open val ${p.name}: ${p.type}?
        @Suppress("UNCHECKED_CAST")
        get() = getInPort("${p.name}").value as? ${p.type}

    </#list>
    <#list viewModel.outPorts as p>
    protected open fun set${p.name?cap_first}(v: ${p.type}) {
        getOutPort("${p.name}").value = v
    }

    </#list>
}
