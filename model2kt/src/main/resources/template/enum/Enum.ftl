<#-- (c) https://github.com/MontiCore/monticore -->
<#include "/Package.ftl">

enum class ${helper.getName(viewModel)} {
<#list viewModel.constants as constant>
  ${constant}<#sep>,</#sep>
</#list>
}
