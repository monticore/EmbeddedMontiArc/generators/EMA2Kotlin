/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.model2kt.template

import de.monticore.lang.monticar.ema2kt.model.ChildOut2ChildInPortConnection
import de.monticore.lang.monticar.ema2kt.model.ChildOut2SelfOutPortConnection
import de.monticore.lang.monticar.ema2kt.model.Component
import de.monticore.lang.monticar.ema2kt.model.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.model.IEntity
import de.monticore.lang.monticar.ema2kt.model.MetaDataForUser
import de.monticore.lang.monticar.ema2kt.model.Parameter
import de.monticore.lang.monticar.ema2kt.model.ParametersMetaData
import de.monticore.lang.monticar.ema2kt.model.PassedParameter
import de.monticore.lang.monticar.ema2kt.model.Port
import de.monticore.lang.monticar.ema2kt.model.SelfIn2ChildInPortConnection
import de.monticore.lang.monticar.ema2kt.model.SelfOut2SelfInPortConnection
import de.monticore.lang.monticar.ema2kt.model.Struct
import de.monticore.lang.monticar.ema2kt.model.StructField
import de.monticore.lang.monticar.ema2kt.model.getInPort
import de.monticore.lang.monticar.ema2kt.model.getOutPort
import freemarker.template.Template
import org.junit.Assert
import org.junit.Test
import java.io.StringWriter

internal class TemplatesCollectionTest {
    val templates = TemplatesCollection.create()

    val e1 = Component(
            listOf("test", "component", "sub1", "sub2", "sub3", "E1"),
            setOf(Port("in1", "Float")),
            setOf(Port("out1", "a.b.c.T1"), Port("out2", "a.b.c.d.e.f.k.test123.T2"))
    )

    val e1Meta = ComponentMetaData(
            listOf("test", "meta", "sub1", "sub2", "sub3", "MetaDataForE1"),
            emptyMap()
    )

    val e2 = Component(
            listOf("test", "component", "sub1", "sub2", "E2"),
            setOf(Port("in1", "Float"), Port("in2", "a.b.c.T1")),
            setOf(Port("out1", "Boolean"), Port("out2", "Int"))
    )

    val e2Meta = ComponentMetaData(
            listOf("test", "meta", "sub1", "sub2", "MetaDataForE2"),
            emptyMap()
    )

    val e3 = Component(
            listOf("test", "component", "sub1", "sub2", "E3"),
            setOf(Port("in1", "Float"), Port("in2", "T1")),
            setOf(Port("out1", "Boolean"), Port("out2", "T2")),
            typeParametersMetaData = ParametersMetaData(
                    parameterNames = mutableSetOf("T1", "T2")
            ),
            parametersMetaData = ParametersMetaData(
                    parameterNames = mutableSetOf("p1", "p2", "p3", "p4", "p5", "p6", "p7"),
                    parameterName2Type = mutableMapOf(
                            "p1" to "String",
                            "p2" to "Boolean",
                            "p3" to "Int",
                            "p4" to "Double",
                            "p5" to "String",
                            "p6" to "T2",
                            "p7" to "T1"
                    ),
                    defaultValuesOfParameters = mutableListOf(
                            Parameter("p1", "string \"mom\" is palindrome"),
                            Parameter("p2", true),
                            Parameter("p3", 12),
                            Parameter("p4", -0.87),
                            Parameter("p5", "<strict>")
                    )
            )
    )

    val e3Meta = ComponentMetaData(
            listOf("test", "meta", "sub1", "sub2", "MetaDataForE3"),
            emptyMap()
    )

    val c1 = Component(
            listOf("test", "component", "sub1", "sub2", "C1"),
            setOf(
                    Port("in1", "Float"),
                    Port("in2", "java.math.BigDecimal"),
                    Port("in3", "Boolean")
            ),
            setOf(
                    Port("out1", "a.b.c.T1"),
                    Port("out2", "a.b.c.d.e.f.k.test123.T2"),
                    Port("out3", "Boolean")
            ),
            children = mutableMapOf("e1" to e1, "e2" to e2)
    )

    val c1Meta = ComponentMetaData(
            listOf("test", "meta", "sub1", "sub2", "MetaDataForC1"),
            mapOf("e1" to e1Meta, "e2" to e2Meta)
    )

    val c2 = Component(
            listOf("test", "component", "sub1", "sub2", "C2"),
            setOf(
                    Port("in1", "Float"),
                    Port("in2", "java.math.BigDecimal"),
                    Port("in3", "Boolean")
            ),
            setOf(
                    Port("out1", "a.b.c.T1"),
                    Port("out2", "a.b.c.d.e.f.k.test123.T2"),
                    Port("out3", "Boolean")
            ),
            typeParametersMetaData = ParametersMetaData(
                    parameterNames = mutableSetOf("T"),
                    parametersPassed2Children = mutableListOf(
                            PassedParameter("T", "e31", "T1"),
                            PassedParameter("T", "e32", "T2")
                    ),
                    literalInitializationParametersForChildren = mutableMapOf(
                            "e31" to listOf(Parameter("T2", "Double")),
                            "e32" to listOf(Parameter("T1", "foo.bar.Baz"))
                    )
            ),
            parametersMetaData = ParametersMetaData(
                    parameterNames = mutableSetOf("p1", "p2", "p3", "p4", "p5", "p6", "p7"),
                    defaultValuesOfParameters = mutableListOf(
                            Parameter("p1", "string \"loop\" is not palindrome"),
                            Parameter("p2", 11),
                            Parameter("p3", 1.22),
                            Parameter("p4", true),
                            Parameter("p5", -7.12)
                    ),
                    parametersPassed2Children = mutableListOf(
                            PassedParameter("p1", "e31", "p1"),
                            PassedParameter("p1", "e32", "p5"),
                            PassedParameter("p2", "e31", "p3"),
                            PassedParameter("p3", "e32", "p4"),
                            PassedParameter("p4", "e31", "p2")
                    ),
                    literalInitializationParametersForChildren = mutableMapOf(
                            "e31" to mutableListOf(
                                    Parameter("p4", false)
                            ),
                            "e32" to listOf(
                                    Parameter("p1", "hi!"),
                                    Parameter("p2", true),
                                    Parameter("p3", 100)
                            )
                    )
            ),
            children = mutableMapOf("e1" to e1, "e2" to e2, "e31" to e3, "e32" to e3)
    )

    val c2Meta = ComponentMetaData(
            listOf("test", "meta", "sub1", "sub2", "MetaDataForC2"),
            mapOf("e1" to e1Meta, "e2" to e2Meta, "e31" to e3Meta, "e32" to e3Meta)
    )

    val metaForUser = MetaDataForUser(
            listOf("test", "meta", "ToBeImplemented"),
            listOf(e1Meta, e2Meta, e3Meta)
    )

    init {
        e1.metaData = e1Meta
        e1Meta.component = e1
        e2.metaData = e2Meta
        e2Meta.component = e2
        e3.metaData = e3Meta
        e3Meta.component = e3
        c1.metaData = c1Meta
        c1Meta.component = c1
        val c1_e1 = SelfIn2ChildInPortConnection(
                c1.getInPort("in1")!!,
                "e1",
                e1.getInPort("in1")!!
        )
        val e1_e2 = ChildOut2ChildInPortConnection(
                "e1",
                e1.getOutPort("out1")!!,
                "e2",
                e2.getInPort("in2")!!
        )
        val e2_c1 = ChildOut2SelfOutPortConnection(
                "e2",
                e2.getOutPort("out1")!!,
                c1.getOutPort("out3")!!
        )
        val c1_c1 = SelfOut2SelfInPortConnection(
                c1.getOutPort("out3")!!,
                c1.getInPort("in3")!!
        )
        c1.portConnections.addAll(setOf(c1_e1, e1_e2, e2_c1, c1_c1))
        c2.metaData = c2Meta
        c2Meta.component = c2
        c2.portConnections.addAll(setOf(c1_e1, e1_e2, e2_c1, c1_c1))
    }

    @Test
    fun testStructureGeneration() {
        val viewModel = Struct(
                listOf("test", "sub1", "sub2", "MyCoolStruct1"),
                setOf(
                        StructField("f1", "Boolean", ""),
                        StructField("f2", "Double", "1.0"),
                        StructField("f3", "a.b.c.SomeStruct1", ""),
                        StructField("f4", "List<a.b.c.SomeStruct2>", ""),
                        StructField("f5", "List<List<List<a.b.c.SomeStruct3>>>", "")
                )
        )
        val expectedFragments = listOf(
                "package test.sub1.sub2",
                "data class MyCoolStruct1",
                "var f1: Boolean",
                "var f2: Double = 1.0",
                "var f3: a.b.c.SomeStruct1",
                "var f4: List<a.b.c.SomeStruct2>",
                "var f5: List<List<List<a.b.c.SomeStruct3>>>"
        )
        checkResultContainsExpectedFragments(
                templates.templateForStructure,
                viewModel,
                expectedFragments
        )
    }

    @Test
    fun testComponentMetaDataGeneration() {
        val expectedFragments = listOf(
                "package test.meta.sub1.sub2",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.AbstractComponentMetaData",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.PortConnection",
                "object MetaDataForC1 : AbstractComponentMetaData",
                "componentId = \"test.component.sub1.sub2.C1\"",
                "\"in1\"", "\"in2\"", "\"in3\"",
                "\"out1\"", "\"out2\"", "\"out3\"",
                "\"e1\" to test.meta.sub1.sub2.sub3.MetaDataForE1",
                "\"e2\" to test.meta.sub1.sub2.MetaDataForE2",
                "PortConnection.self2Child(\"in1\", \"e1\", \"in1\")",
                "PortConnection.child2Child(\"e1\", \"out1\", \"e2\", \"in2\")",
                "PortConnection.child2Self(\"e2\", \"out1\", \"out3\")",
                "PortConnection.selfOut2SelfIn(\"out3\", \"in3\")"
        )
        checkResultContainsExpectedFragments(
                templates.templateForComponentMetaData,
                c1Meta,
                expectedFragments
        )
    }

    @Test
    fun testUserMetaDataGeneration() {
        val expectedFragments = listOf(
                "package test.meta",
                "object ToBeImplemented",
                "val metaForE1 = test.meta.sub1.sub2.sub3.MetaDataForE1",
                "val idOfE1 = metaForE1.componentId",
                "val metaForE2 = test.meta.sub1.sub2.MetaDataForE2",
                "val idOfE2 = metaForE2.componentId",
                "val allMetaData = listOf",
                "val allIds = setOf"
        )
        checkResultContainsExpectedFragments(
                templates.templateForUserMetaData,
                metaForUser,
                expectedFragments
        )
    }

    @Test
    fun testComponentGeneration1() {
        val expectedFragments = listOf(
                "package test.component.sub1.sub2.sub3",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.AbstractUserImplementedComponent",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.Port",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.getInPort",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.getOutPort",
                "abstract class E1",
                "inPorts = Port.createMany(test.meta.sub1.sub2.sub3.MetaDataForE1.inPortNames)",
                "outPorts = Port.createMany(test.meta.sub1.sub2.sub3.MetaDataForE1.outPortNames)",
                "protected open val in1: Float?",
                "get() = getInPort(\"in1\").value as? Float",
                "protected open fun setOut1(v: a.b.c.T1)",
                "getOutPort(\"out1\").value = v",
                "protected open fun setOut2(v: a.b.c.d.e.f.k.test123.T2)",
                "getOutPort(\"out2\").value = v"
        )
        checkResultContainsExpectedFragments(
                templates.templateForComponent,
                e1,
                expectedFragments
        )
    }

    @Test
    fun testComponentGeneration2() {
        val expectedFragments = listOf(
                "package test.component.sub1.sub2",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.AbstractUserImplementedComponent",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.Port",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.getInPort",
                "import de.monticore.lang.monticar.ema2kt.runtime.api.getOutPort",
                "abstract class E2",
                "inPorts = Port.createMany(test.meta.sub1.sub2.MetaDataForE2.inPortNames)",
                "outPorts = Port.createMany(test.meta.sub1.sub2.MetaDataForE2.outPortNames)",
                "protected open val in1: Float?",
                "get() = getInPort(\"in1\").value as? Float",
                "protected open val in2: a.b.c.T1?",
                "get() = getInPort(\"in2\").value as? a.b.c.T1",
                "protected open fun setOut1(v: Boolean)",
                "getOutPort(\"out1\").value = v",
                "protected open fun setOut2(v: Int)",
                "getOutPort(\"out2\").value = v"
        )
        checkResultContainsExpectedFragments(
                templates.templateForComponent,
                e2,
                expectedFragments
        )
    }

    @Test
    fun testTypeParametersGenerationInMetaData() {
        val expectedFragments = listOf(
                "typeParametersMetaData = ParametersMetaData.create",
                "parameters = TypeParametersOfC2.all",
                "PassedParameter.create(TypeParametersOfC2.T, \"e31\", \"T1\")",
                "PassedParameter.create(TypeParametersOfC2.T, \"e32\", \"T2\")",
                "Parameter.create(\"T2\", \"Double\")",
                "Parameter.create(\"T1\", \"foo.bar.Baz\")"
        )
        checkResultContainsExpectedFragments(
                templates.templateForComponentMetaData,
                c2Meta,
                expectedFragments
        )
    }

    @Test
    fun testConfigurationParametersGenerationInMetaData() {
        val expectedFragments = listOf(
                "parameters = ConfigurationParametersOfC2.all",
                "Parameter.create(\"p1\", \"string \\\"loop\\\" is not palindrome\")",
                "Parameter.create(\"p2\", 11)",
                "Parameter.create(\"p3\", 1.22)",
                "Parameter.create(\"p4\", true)",
                "Parameter.create(\"p5\", -7.12)",
                "PassedParameter.create(ConfigurationParametersOfC2.p1, \"e31\", \"p1\")",
                "PassedParameter.create(ConfigurationParametersOfC2.p1, \"e32\", \"p5\")",
                "PassedParameter.create(ConfigurationParametersOfC2.p2, \"e31\", \"p3\")",
                "PassedParameter.create(ConfigurationParametersOfC2.p3, \"e32\", \"p4\")",
                "PassedParameter.create(ConfigurationParametersOfC2.p4, \"e31\", \"p2\")",
                "Parameter.create(\"p4\", false)",
                "Parameter.create(\"p1\", \"hi!\")",
                "Parameter.create(\"p2\", true)",
                "Parameter.create(\"p3\", 100)"
        )
        checkResultContainsExpectedFragments(
                templates.templateForComponentMetaData,
                c2Meta,
                expectedFragments
        )
    }

    @Test
    fun testTypeParametersGenerationInComponent() {
        val expectedFragments = listOf(
                "abstract class E3<T1,T2>",
                "protected open val in2: T1?",
                "get() = getInPort(\"in2\").value as? T1",
                "protected open fun setOut2(v: T2) {"
        )
        checkResultContainsExpectedFragments(
                templates.templateForComponent,
                e3,
                expectedFragments
        )
    }

    @Test
    fun testParametersGenerationInComponent() {
        val expectedFragments = listOf(
                "protected open val p1 : String = \"string \\\"mom\\\" is palindrome\"",
                "protected open val p2 : Boolean = true",
                "protected open val p3 : Int = 12",
                "protected open val p4 : Double = -0.87",
                "protected open val p5 : String = \"<strict>\"",
                "protected open val p6 : T2",
                "protected open val p7 : T1"
        )
        checkResultContainsExpectedFragments(
                templates.templateForComponent,
                e3,
                expectedFragments
        )
    }

    private fun checkResultContainsExpectedFragments(
            template: Template,
            viewModel: IEntity,
            expectedFragments: List<String>
    ) {
        val writer = StringWriter()
        template.process(Helper.getDataForTemplate(viewModel), writer)
        val result = writer.toString()
        expectedFragments.forEach {
            Assert.assertTrue(result.contains(it))
        }
    }
}
