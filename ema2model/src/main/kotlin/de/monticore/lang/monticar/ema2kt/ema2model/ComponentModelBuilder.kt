/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol
import de.monticore.lang.monticar.ema2kt.model.ChildOut2ChildInPortConnection
import de.monticore.lang.monticar.ema2kt.model.ChildOut2SelfOutPortConnection
import de.monticore.lang.monticar.ema2kt.model.Component
import de.monticore.lang.monticar.ema2kt.model.EnumViewModel
import de.monticore.lang.monticar.ema2kt.model.IPortConnection
import de.monticore.lang.monticar.ema2kt.model.Parameter
import de.monticore.lang.monticar.ema2kt.model.Port
import de.monticore.lang.monticar.ema2kt.model.SelfIn2ChildInPortConnection
import de.monticore.lang.monticar.ema2kt.model.SelfOut2SelfInPortConnection
import de.monticore.lang.monticar.ema2kt.model.Struct
import de.monticore.lang.monticar.ema2kt.model.fullyQualifiedName
import de.monticore.lang.monticar.ema2kt.model.toFullyQualifiedName
import de.monticore.lang.monticar.ts.MCTypeSymbol
import de.monticore.lang.monticar.ts.references.MCTypeReference
import de.se_rwth.commons.logging.Log

internal class ComponentModelBuilder(
        settings: ModelBuilderSettings,
        private val enums: Map<String, EnumViewModel>,
        private val structures: Map<String, Struct>,
        componentsBeingBuild: MutableMap<String, Component>
) : AbstractModelBuilder<ComponentSymbol, Component>(
        settings,
        componentsBeingBuild
) {
    override fun getFullyQualifiedNameParts(originalFullyQualifiedNameParts: List<String>): List<String> {
        return PackageNamingRules.getComponentFullyQualifiedNameParts(
                settings.generalPackagePrefixParts,
                originalFullyQualifiedNameParts
        )
    }

    override fun checkIfInputIsValid(s: ComponentSymbol) {
        super.checkIfInputIsValid(s)
        val originalFullyQualifiedName = s.fullName
        if (s.isInnerComponent) {
            throw MappingException(
                    "inner components are not supported," +
                            " component: $originalFullyQualifiedName"
            )
        }
        val hasChildren = s.subComponents.isNotEmpty()
        val hasPortConnections = s.connectors.isNotEmpty()
        if (hasChildren.xor(hasPortConnections)) {
            throw MappingException(
                    "child components and connections" +
                            " must be either both present" +
                            " or both absent," +
                            " component $originalFullyQualifiedName"
            )
        }
        if (s.connectors.any { it.isConstant }) {
            throw MappingException(
                    "constant connectors are not supported," +
                            " component $originalFullyQualifiedName"
            )
        }
    }

    override fun buildNewModel(fullyQualifiedNameParts: List<String>, s: ComponentSymbol): Component {
        val inPorts = s.incomingPorts
                .map { Port(it.name, determineType(it.typeReference)) }
                .toMutableSet()
        val outPorts = s.outgoingPorts
                .map { Port(it.name, determineType(it.typeReference)) }
                .toMutableSet()
        val model = Component(
                fullyQualifiedNameParts.toMutableList(),
                inPorts,
                outPorts
        )
        if (s.subComponents.isNotEmpty()) {
            model.children.putAll(s.subComponents.map {
                it.name to buildModel(it.componentType.referencedSymbol)
            })
            model.portConnections.addAll(
                    PortConnectionsBuilder(model).build(s.connectors)
            )
            model.checkPortConnections()
        }
        val cps = ComponentParametersSubstitutionsBuilder(s).build()
        val errors = Log.getFindings().filter { it.isError }
        if (errors.isNotEmpty()) {
            throw MappingException(
                    "cannot process parameters of component ${s.fullName}: $errors"
            )
        }
        val formalSubs = cps.formalTypeParametersSubstitutions
        val typeParamsMeta = model.typeParametersMetaData
        typeParamsMeta.parameterNames.addAll(
                formalSubs.parentFormalTypeParameterNames
        )
        typeParamsMeta.parametersPassed2Children.addAll(
                formalSubs.parametersPassedToChildren
        )
        typeParamsMeta.literalInitializationParametersForChildren.putAll(
                formalSubs.literalChildParameters.map { (childName, params) ->
                    childName to params.map {
                        Parameter(it.parameterName, determineType(it.literalValue))
                    }
                }
        )
        val configSubs = cps.configurationParametersSubstitutions
        val configParamsMeta = model.parametersMetaData
        configParamsMeta.parameterNames.addAll(
                configSubs.parentConfigurationParameters.map { it.key }
        )
        configParamsMeta.parameterName2Type.putAll(
                configSubs.parentConfigurationParameters.map {
                    // TODO type arguments resolution does not work properly
                    val ref = it.value
                    val type = if (formalSubs.parentFormalTypeParameterNames.contains(ref.name)) {
                        ref.name
                    } else {
                        determineType(ref)
                    }
                    it.key to type
                }
        )
        configParamsMeta.parametersPassed2Children.addAll(
                configSubs.parametersPassedToChildren
        )
        configParamsMeta.literalInitializationParametersForChildren.putAll(
                configSubs.literalChildParameters.map {
                    it.key to it.value.map {
                        Parameter(it.parameterName, it.literalValue)
                    }
                }
        )
        return model
    }

    private fun determineType(typeInfo: MCTypeReference<out MCTypeSymbol>): String {
        val ref = typeInfo.referencedSymbol
        val fullName = ref.fullName
        val mappedName = when {
            fullName == "B" -> "Boolean"
            fullName == "Q" -> "Double"
            fullName == "Z" -> "Long"
            fullName == "C" -> "org.apache.commons.math3.complex.Complex"
            ref.isFormalTypeParameter -> ref.name
            else -> {
                val originalFullyQualifiedNameParts = fullName.splitIntoPartsAndCheckIfValid()
                val structFullyQualifiedName = PackageNamingRules.getStructureFullyQualifiedNameParts(
                        settings.generalPackagePrefixParts,
                        originalFullyQualifiedNameParts
                ).toFullyQualifiedName()
                val struct = structures[structFullyQualifiedName]
                if (struct != null) {
                    struct.fullyQualifiedName
                } else {
                    val enumFullyQualifiedName = PackageNamingRules.getEnumFullyQualifiedNameParts(
                            settings.generalPackagePrefixParts,
                            originalFullyQualifiedNameParts
                    ).toFullyQualifiedName()
                    enums[enumFullyQualifiedName]?.fullyQualifiedName
                            ?: throw MappingException("cannot resolve type $fullName")
                }
            }
        }
        if (typeInfo.dimension > 0) {
            return mappedName.asListOfDimension(typeInfo.dimension)
        }
        return mappedName
    }
}

private val SPECIAL_NAME_FOR_PARENT_COMPONENT = ""

private fun Component.checkPortConnections() {
    val socketsHavingConnection = mutableMapOf<Pair<String, String>, IPortConnection>()
    portConnections.forEach {
        val socket = when (it) {
            is SelfIn2ChildInPortConnection ->
                Pair(it.targetChild, it.targetInPort.name)
            is ChildOut2ChildInPortConnection ->
                Pair(it.targetChild, it.targetInPort.name)
            is ChildOut2SelfOutPortConnection ->
                Pair(SPECIAL_NAME_FOR_PARENT_COMPONENT, it.targetOutPort.name)
            is SelfOut2SelfInPortConnection ->
                Pair(SPECIAL_NAME_FOR_PARENT_COMPONENT, it.targetInPort.name)
            else -> {
                throw MappingException("unknown port connection $it")
            }
        }
        val existingConnection = socketsHavingConnection[socket]
        if (existingConnection != null) {
            throw MappingException(
                    "target port in port connection $it" +
                            " receives already data" +
                            " from another connection $existingConnection," +
                            " parent component: $this"
            )
        }
        socketsHavingConnection.put(socket, it)
    }
}
