/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.monticar.ema2kt.model.PassedParameter
import de.monticore.lang.monticar.ts.MCTypeSymbol
import de.monticore.lang.monticar.ts.references.MCTypeReference

class ConfigurationParametersSubstitutions(
        val parentConfigurationParameters: MutableMap<String, MCTypeReference<out MCTypeSymbol>>
        = mutableMapOf(),
        val parametersPassedToChildren: MutableList<PassedParameter>
        = mutableListOf(),
        val literalChildParameters: MutableMap<String, MutableList<LiteralParameterSubstitution<Any>>>
        = mutableMapOf()
)
