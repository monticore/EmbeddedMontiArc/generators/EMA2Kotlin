/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.monticar.ema2kt.model.IEntity
import de.monticore.lang.monticar.ema2kt.model.toFullyQualifiedName
import de.monticore.symboltable.Symbol

internal abstract class AbstractModelBuilder<in TSymbol : Symbol, TModel : IEntity>(
        protected val settings: ModelBuilderSettings,
        protected val modelsBeingBuild: MutableMap<String, TModel>
) {
    open protected val modelsUnderConstruction = mutableSetOf<String>()

    init {
        if (settings.generalPackagePrefixParts.isNotEmpty()) {
            settings.generalPackagePrefixParts.checkIfNamesAreValid()
        }
    }

    open fun buildModel(s: TSymbol): TModel {
        val originalFullyQualifiedName = s.fullName
        val originalFullyQualifiedNameParts = originalFullyQualifiedName.splitIntoPartsAndCheckIfValid()
        val fullyQualifiedNameParts = getFullyQualifiedNameParts(originalFullyQualifiedNameParts)
        val fullyQualifiedName = fullyQualifiedNameParts.toFullyQualifiedName()
        val alreadyBuiltModel = modelsBeingBuild[fullyQualifiedName]
        if (alreadyBuiltModel != null) {
            return alreadyBuiltModel
        }
        if (!modelsUnderConstruction.add(fullyQualifiedName)) {
            throw MappingException(
                    "model references form a loop: $modelsUnderConstruction"
            )
        }
        checkIfInputIsValid(s)
        val model = buildNewModel(fullyQualifiedNameParts, s)
        modelsBeingBuild.put(fullyQualifiedName, model)
        modelsUnderConstruction.remove(fullyQualifiedName)
        return model
    }

    abstract protected fun getFullyQualifiedNameParts(originalFullyQualifiedNameParts: List<String>): List<String>

    open protected fun checkIfInputIsValid(s: TSymbol) {}

    abstract protected fun buildNewModel(fullyQualifiedNameParts: List<String>, s: TSymbol): TModel
}
