/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.monticar.ema2kt.model.Component
import de.monticore.lang.monticar.ema2kt.model.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.model.fullyQualifiedName

internal class ComponentMetaDataBuilder(
        private val settings: ModelBuilderSettings,
        private val componentFullName2MetaData: MutableMap<String, ComponentMetaData>
) {
    private val modelsUnderConstruction = mutableSetOf<String>()

    fun buildComponentMetaData(component: Component): ComponentMetaData {
        val alreadyBuiltMeta = componentFullName2MetaData[component.fullyQualifiedName]
        if (alreadyBuiltMeta != null) {
            return alreadyBuiltMeta
        }
        if (!modelsUnderConstruction.add(component.fullyQualifiedName)) {
            throw MappingException(
                    "model references form a loop: $modelsUnderConstruction"
            )
        }
        val fullyQualifiedNameParts = PackageNamingRules.getComponentMetaDataFullyQualifiedNameParts(
                settings.generalPackagePrefixParts,
                component
        )
        val childrenMeta = component.children
                .mapValues { buildComponentMetaData(it.value) }
                .toMutableMap()
        val meta = ComponentMetaData(fullyQualifiedNameParts, childrenMeta)
        meta.component = component
        component.metaData = meta
        componentFullName2MetaData.put(component.fullyQualifiedName, meta)
        modelsUnderConstruction.remove(component.fullyQualifiedName)
        return meta
    }
}
