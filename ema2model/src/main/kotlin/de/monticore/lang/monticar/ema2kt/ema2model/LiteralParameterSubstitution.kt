/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

class LiteralParameterSubstitution<out T>(
        val parameterName: String,
        val literalValue: T
)
