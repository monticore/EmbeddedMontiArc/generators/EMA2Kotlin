/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.monticar.ema2kt.model.EnumViewModel
import de.monticore.lang.monticar.enumlang._symboltable.EnumDeclarationSymbol

internal class EnumModelBuilder(
        settings: ModelBuilderSettings,
        enumsBeingBuild: MutableMap<String, EnumViewModel>
) : AbstractModelBuilder<EnumDeclarationSymbol, EnumViewModel>(
        settings,
        enumsBeingBuild
) {
    override fun getFullyQualifiedNameParts(originalFullyQualifiedNameParts: List<String>): List<String> {
        return PackageNamingRules.getEnumFullyQualifiedNameParts(
                settings.generalPackagePrefixParts,
                originalFullyQualifiedNameParts
        )
    }

    override fun buildNewModel(fullyQualifiedNameParts: List<String>, s: EnumDeclarationSymbol): EnumViewModel {
        return EnumViewModel(
                fullyQualifiedNameParts,
                s.enumConstants.map { it.name }.toSet()
        )
    }
}
