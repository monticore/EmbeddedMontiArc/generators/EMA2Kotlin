/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

data class ModelBuilderSettings(
        val generalPackagePrefixParts: List<String> = emptyList()
)
