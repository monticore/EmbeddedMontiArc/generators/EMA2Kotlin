/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbol
import de.monticore.lang.monticar.ema2kt.model.Component
import de.monticore.lang.monticar.ema2kt.model.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.model.DomainModel
import de.monticore.lang.monticar.ema2kt.model.EnumViewModel
import de.monticore.lang.monticar.ema2kt.model.MetaDataForUser
import de.monticore.lang.monticar.ema2kt.model.Struct
import de.monticore.lang.monticar.enumlang._symboltable.EnumDeclarationSymbol
import de.monticore.lang.monticar.struct._symboltable.StructSymbol
import de.monticore.symboltable.Scope

internal class ModelBuilderImplementation(
        private val settings: ModelBuilderSettings
) : ModelBuilder {
    private val enums = mutableMapOf<String, EnumViewModel>()
    private val structures = mutableMapOf<String, Struct>()
    private val components = mutableMapOf<String, Component>()
    private val componentsMetaData = mutableMapOf<String, ComponentMetaData>()

    override fun buildModels(symbolTable: Scope): DomainModel {
        enums.clear()
        structures.clear()
        components.clear()
        componentsMetaData.clear()
        buildEnums(symbolTable)
        buildStructures(symbolTable)
        buildComponents(symbolTable)
        buildComponentsMetaData()
        val componentsMeta = componentsMetaData.values.toList()
        val metaForUser = MetaDataForUser(
                PackageNamingRules.getMetaDataForUserFullyQualifiedNameParts(
                        settings.generalPackagePrefixParts
                ),
                componentsMeta.filter { it.children.isEmpty() }
        )
        return DomainModel(
                enums.values.toList(),
                structures.values.toList(),
                components.values.toList(),
                componentsMeta,
                metaForUser
        )
    }

    private fun buildEnums(scope: Scope) {
        scope.localSymbols
                .flatMap { it.value.toList() }
                .filterIsInstance<EnumDeclarationSymbol>()
                .forEach {
                    EnumModelBuilder(
                            settings,
                            enums
                    ).buildModel(it)
                }
        scope.subScopes.forEach { buildEnums(it) }
    }


    private fun buildStructures(scope: Scope) {
        val structSymbols = scope.localSymbols
                .flatMap { it.value.toList() }
                .filterIsInstance<StructSymbol>()
        structSymbols.forEach {
            val builder = StructureModelBuilder(
                    settings,
                    enums,
                    structures
            )
            builder.buildModel(it)
        }
        scope.subScopes.forEach { buildStructures(it) }
    }

    private fun buildComponents(scope: Scope) {
        val componentsSymbols = scope.localSymbols
                .flatMap { it.value.toList() }
                .filterIsInstance<ComponentSymbol>()
        componentsSymbols.forEach {
            val builder = ComponentModelBuilder(
                    settings,
                    enums,
                    structures,
                    components
            )
            builder.buildModel(it)
        }
        scope.subScopes.forEach { buildComponents(it) }
    }

    private fun buildComponentsMetaData() {
        val builder = ComponentMetaDataBuilder(settings, componentsMetaData)
        components.values.forEach { builder.buildComponentMetaData(it) }
    }
}
