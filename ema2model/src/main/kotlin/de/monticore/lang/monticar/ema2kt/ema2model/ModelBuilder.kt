/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.monticar.ema2kt.model.DomainModel
import de.monticore.symboltable.Scope

interface ModelBuilder {
    fun buildModels(symbolTable: Scope): DomainModel

    companion object {
        fun create(): ModelBuilder {
            return create(ModelBuilderSettings())
        }

        fun create(settings: ModelBuilderSettings): ModelBuilder {
            return ModelBuilderImplementation(settings)
        }
    }
}
