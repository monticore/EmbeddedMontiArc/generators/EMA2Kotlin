/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

class ComponentParametersSubstitutions(
        val formalTypeParametersSubstitutions: FormalTypeParametersSubstitutions,
        val configurationParametersSubstitutions: ConfigurationParametersSubstitutions
)
