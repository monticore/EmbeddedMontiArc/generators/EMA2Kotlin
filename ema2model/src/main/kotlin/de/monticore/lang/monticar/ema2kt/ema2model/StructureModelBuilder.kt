/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.monticar.ema2kt.model.EnumViewModel
import de.monticore.lang.monticar.ema2kt.model.Struct
import de.monticore.lang.monticar.ema2kt.model.StructField
import de.monticore.lang.monticar.ema2kt.model.fullyQualifiedName
import de.monticore.lang.monticar.ema2kt.model.toFullyQualifiedName
import de.monticore.lang.monticar.enumlang._symboltable.EnumDeclarationSymbol
import de.monticore.lang.monticar.struct._symboltable.StructSymbol
import de.monticore.lang.monticar.ts.MCTypeSymbol
import de.monticore.lang.monticar.ts.references.MCTypeReference

internal class StructureModelBuilder(
        settings: ModelBuilderSettings,
        private val enums: Map<String, EnumViewModel>,
        structuresBeingBuild: MutableMap<String, Struct>
) : AbstractModelBuilder<StructSymbol, Struct>(
        settings,
        structuresBeingBuild
) {
    override fun getFullyQualifiedNameParts(originalFullyQualifiedNameParts: List<String>): List<String> {
        return PackageNamingRules.getStructureFullyQualifiedNameParts(
                settings.generalPackagePrefixParts,
                originalFullyQualifiedNameParts
        )
    }

    override fun checkIfInputIsValid(s: StructSymbol) {
        super.checkIfInputIsValid(s)
        val originalFullyQualifiedName = s.fullName
        if (s.structFieldDefinitions.isEmpty()) {
            throw MappingException(
                    "structure $originalFullyQualifiedName does not have fields"
            )
        }
        s.structFieldDefinitions.map { it.name }.forEach {
            if (!it.isValidName()) {
                throw MappingException(
                        "field name $it is invalid in structure $originalFullyQualifiedName"
                )
            }
        }
    }

    override fun buildNewModel(fullyQualifiedNameParts: List<String>, s: StructSymbol): Struct {
        val fields = s.structFieldDefinitions
                .map { StructField(it.name, determineType(it.type), "") }
                .toMutableSet()
        return Struct(fullyQualifiedNameParts.toMutableList(), fields)
    }

    private fun determineType(typeInfo: MCTypeReference<out MCTypeSymbol>): String {
        val refSymbol = typeInfo.referencedSymbol
        val fullName = refSymbol.fullName
        val mappedName: String = when {
            fullName == "B" -> "Boolean"
            fullName == "Q" -> "Double"
            fullName == "Z" -> "Long"
            fullName == "C" -> "org.apache.commons.math3.complex.Complex"
            refSymbol is EnumDeclarationSymbol -> {
                val enumFullyQualifiedName = PackageNamingRules.getEnumFullyQualifiedNameParts(
                        settings.generalPackagePrefixParts,
                        refSymbol.fullName.splitIntoPartsAndCheckIfValid()
                ).toFullyQualifiedName()
                enums[enumFullyQualifiedName]?.fullyQualifiedName
                        ?: throw MappingException("cannot resolve enum $fullName")
            }
            refSymbol is StructSymbol -> {
                val s = buildModel(refSymbol)
                s.fullyQualifiedName
            }
            else -> throw throw MappingException("unknown type $fullName")
        }
        if (typeInfo.dimension > 0) {
            return mappedName.asListOfDimension(typeInfo.dimension)
        }
        return mappedName
    }
}
