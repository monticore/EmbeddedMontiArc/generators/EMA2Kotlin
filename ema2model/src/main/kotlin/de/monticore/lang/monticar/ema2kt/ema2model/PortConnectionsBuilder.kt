/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol
import de.monticore.lang.monticar.ema2kt.model.ChildOut2ChildInPortConnection
import de.monticore.lang.monticar.ema2kt.model.ChildOut2SelfOutPortConnection
import de.monticore.lang.monticar.ema2kt.model.Component
import de.monticore.lang.monticar.ema2kt.model.FULLY_QUALIFIED_NAME_SEPARATOR
import de.monticore.lang.monticar.ema2kt.model.IPortConnection
import de.monticore.lang.monticar.ema2kt.model.Port
import de.monticore.lang.monticar.ema2kt.model.SelfIn2ChildInPortConnection
import de.monticore.lang.monticar.ema2kt.model.SelfOut2SelfInPortConnection
import de.monticore.lang.monticar.ema2kt.model.fullyQualifiedName
import de.monticore.lang.monticar.ema2kt.model.getChild
import de.monticore.lang.monticar.ema2kt.model.getInPort
import de.monticore.lang.monticar.ema2kt.model.getOutPort

internal class PortConnectionsBuilder(
        private val parentComponent: Component
) {
    fun build(connectors: Iterable<ConnectorSymbol>): List<IPortConnection> {
        return connectors.map { connectorSymbol2PortConnection(it) }
    }

    private fun connectorSymbol2PortConnection(s: ConnectorSymbol): IPortConnection {
        val (sourceChildName, sourcePortName) = s.source.getChildAndPort()
        val (targetChildName, targetPortName) = s.target.getChildAndPort()
        val isSourceSelf = sourceChildName == SPECIAL_NAME_FOR_PARENT_COMPONENT
        val isTargetSelf = targetChildName == SPECIAL_NAME_FOR_PARENT_COMPONENT
        return if (isSourceSelf) {
            if (isTargetSelf) {
                buildSelfOut2SelfInPortConnection(sourcePortName, targetPortName)
            } else {
                buildSelfIn2ChildInPortConnection(sourcePortName, targetChildName, targetPortName)
            }
        } else {
            if (isTargetSelf) {
                buildChildOut2SelfOutPortConnection(sourceChildName, sourcePortName, targetPortName)
            } else {
                buildChild2ChildPortConnection(sourceChildName, sourcePortName, targetChildName, targetPortName)
            }
        }
    }

    private fun buildSelfOut2SelfInPortConnection(
            sourceOutPortName: String,
            targetInPortName: String
    ): SelfOut2SelfInPortConnection {
        val sourceOutPort = parentComponent.getOutPortWhichMustExist(sourceOutPortName)
        val targetInPort = parentComponent.getInPortWhichMustExist(targetInPortName)
        return SelfOut2SelfInPortConnection(sourceOutPort, targetInPort)
    }

    private fun buildSelfIn2ChildInPortConnection(
            sourceInPortName: String,
            targetChildName: String,
            targetInPortName: String
    ): SelfIn2ChildInPortConnection {
        val sourceInPort = parentComponent.getInPortWhichMustExist(sourceInPortName)
        val targetInPort = parentComponent.getChildWhichMustExist(targetChildName).getInPortWhichMustExist(targetInPortName)
        return SelfIn2ChildInPortConnection(sourceInPort, targetChildName, targetInPort)
    }

    private fun buildChildOut2SelfOutPortConnection(
            sourceChildName: String,
            sourceOutPortName: String,
            targetOutPortName: String
    ): ChildOut2SelfOutPortConnection {
        val sourceOutPort = parentComponent.getChildWhichMustExist(sourceChildName).getOutPortWhichMustExist(sourceOutPortName)
        val targetOutPort = parentComponent.getOutPortWhichMustExist(targetOutPortName)
        return ChildOut2SelfOutPortConnection(sourceChildName, sourceOutPort, targetOutPort)
    }

    private fun buildChild2ChildPortConnection(
            sourceChildName: String,
            sourceOutPortName: String,
            targetChildName: String,
            targetInPortName: String
    ): ChildOut2ChildInPortConnection {
        val sourceOutPort = parentComponent.getChildWhichMustExist(sourceChildName).getOutPortWhichMustExist(sourceOutPortName)
        val targetInPort = parentComponent.getChildWhichMustExist(targetChildName).getInPortWhichMustExist(targetInPortName)
        return ChildOut2ChildInPortConnection(sourceChildName, sourceOutPort, targetChildName, targetInPort)
    }
}

private val SPECIAL_NAME_FOR_PARENT_COMPONENT = ""

private fun String.getChildAndPort(): Pair<String, String> {
    val parts = split(FULLY_QUALIFIED_NAME_SEPARATOR)
    if (parts.size == 1) {
        return Pair(SPECIAL_NAME_FOR_PARENT_COMPONENT, parts[0])
    }
    if (parts.size == 2) {
        return Pair(parts[0], parts[1])
    }
    throw MappingException("wrong port connection format: $this")
}

private fun Component.getInPortWhichMustExist(portName: String): Port {
    return getInPort(portName) ?: throw MappingException(
            "component $fullyQualifiedName does not have input port $portName"
    )
}

private fun Component.getOutPortWhichMustExist(portName: String): Port {
    return getOutPort(portName) ?: throw MappingException(
            "component $fullyQualifiedName does not have output port $portName"
    )
}

private fun Component.getChildWhichMustExist(name: String): Component {
    return getChild(name) ?: throw MappingException(
            "component $fullyQualifiedName does not have child $name"
    )
}
