/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.ast.ASTNode

object StartSourcePositionComparator : Comparator<ASTNode> {
    override fun compare(o1: ASTNode, o2: ASTNode): Int {
        val l1 = o1._SourcePositionStart.line
        val l2 = o2._SourcePositionStart.line
        val lResult = l1.compareTo(l2)
        if (lResult != 0) {
            return lResult
        }
        val c1 = o1._SourcePositionStart.column
        val c2 = o2._SourcePositionStart.column
        return c1.compareTo(c2)
    }
}
