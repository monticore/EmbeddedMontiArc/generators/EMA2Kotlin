/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.monticar.ema2kt.model.Component
import de.monticore.lang.monticar.ema2kt.model.name
import de.monticore.lang.monticar.ema2kt.model.packageNameParts
import de.monticore.lang.monticar.ema2kt.model.splitIntoParts
import de.monticore.lang.monticar.ema2kt.model.toFullyQualifiedName

private val VALID_NAME_REGEX = Regex("[a-zA-Z]+[a-zA-Z0-9_]*")
private val KOTLIN_RESERVED_WORDS = setOf(
        "as", "break", "class", "continue", "do", "else", "false",
        "for", "fun", "if", "in", "interface", "is", "null", "object",
        "package", "return", "super", "this", "throw", "true", "try",
        "typealias", "val", "var", "when", "while",
        "by", "catch", "constructor", "delegate", "dynamic", "field", "file", "finally",
        "get", "import", "init", "param", "property", "receiver", "set", "setparam", "where",
        "annotation", "abstract", "companion", "const", "crossinline",
        "data", "enum", "external", "final", "infix", "inline", "inner",
        "internal", "lateinit", "noinline", "open", "operator", "out", "override",
        "private", "protected", "public", "reified", "sealed", "suspend", "tailrec", "vararg",
        "it"
)

internal fun String.isValidName(): Boolean {
    return VALID_NAME_REGEX.matches(this) && !KOTLIN_RESERVED_WORDS.contains(toLowerCase())
}

internal fun Iterable<String>.isValidNames(): Boolean {
    return all { it.isValidName() }
}

internal fun List<String>.checkIfNamesAreValid() {
    if (isEmpty() || !isValidNames()) {
        throw MappingException(
                "$this does not form valid fully qualified name"
        )
    }
}

internal fun String.splitIntoPartsAndCheckIfValid(): List<String> {
    val parts = splitIntoParts()
    parts.checkIfNamesAreValid()
    return parts
}

internal fun String.toComponentFullyQualifiedName(generalPackagePrefixParts: List<String>): String {
    return PackageNamingRules.getComponentFullyQualifiedNameParts(
            generalPackagePrefixParts,
            splitIntoPartsAndCheckIfValid()
    ).toFullyQualifiedName()
}

internal object PackageNamingRules {
    val enumsPackage = "enums"
    val structuresPackage = "structure"
    val componentsPackage = "component"
    val metaDataPackage = "meta"

    fun getEnumFullyQualifiedNameParts(
            generalPackagePrefixParts: List<String>,
            enumOriginalNameParts: List<String>
    ): List<String> {
        return getFullyQualifiedNameParts(generalPackagePrefixParts, enumsPackage, enumOriginalNameParts)
    }

    fun getStructureFullyQualifiedNameParts(
            generalPackagePrefixParts: List<String>,
            structureOriginalNameParts: List<String>
    ): List<String> {
        return getFullyQualifiedNameParts(generalPackagePrefixParts, structuresPackage, structureOriginalNameParts)
    }

    fun getComponentFullyQualifiedNameParts(
            generalPackagePrefixParts: List<String>,
            componentOriginalNameParts: List<String>
    ): List<String> {
        return getFullyQualifiedNameParts(generalPackagePrefixParts, componentsPackage, componentOriginalNameParts)
    }

    fun getComponentMetaDataFullyQualifiedNameParts(
            generalPackagePrefixParts: List<String>,
            component: Component
    ): List<String> {
        val metaNameParts = component.packageNameParts.plus("MetaDataFor${component.name}")
        return getFullyQualifiedNameParts(generalPackagePrefixParts, metaDataPackage, metaNameParts)
    }

    fun getMetaDataForUserFullyQualifiedNameParts(
            generalPackagePrefixParts: List<String>
    ): List<String> {
        val metaNameParts = listOf("ToBeImplemented")
        return getFullyQualifiedNameParts(generalPackagePrefixParts, metaDataPackage, metaNameParts)
    }

    private fun getFullyQualifiedNameParts(
            prefix: List<String>,
            infix: String,
            postfix: List<String>
    ): List<String> {
        return prefix.plus(infix).plus(postfix)
    }
}

internal fun String.asListOfDimension(dimension: Int): String {
    val prefix = "List<".repeat(dimension)
    val postfix = ">".repeat(dimension)
    return "$prefix$this$postfix"
}
