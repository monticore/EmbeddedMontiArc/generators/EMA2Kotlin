/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.lang.monticar.ema2kt.model.PassedParameter
import de.monticore.lang.monticar.ts.MCTypeSymbol
import de.monticore.lang.monticar.ts.references.MCTypeReference

class FormalTypeParametersSubstitutions(
        val parentFormalTypeParameterNames: MutableSet<String>
        = mutableSetOf(),
        val parametersPassedToChildren: MutableList<PassedParameter>
        = mutableListOf(),
        val literalChildParameters: MutableMap<String, MutableList<LiteralParameterSubstitution<MCTypeReference<out MCTypeSymbol>>>>
        = mutableMapOf()
)
