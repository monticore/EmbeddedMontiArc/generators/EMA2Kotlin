/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.ema2model

import de.monticore.ast.ASTNode

internal fun <T> Iterable<T>.asSortedByStartSourcePosition(): MutableList<T>
        where T : ASTNode {
    val result = toMutableList()
    result.sortWith(StartSourcePositionComparator)
    return result
}
