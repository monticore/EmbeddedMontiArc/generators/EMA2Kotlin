/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.jinteroptest;

import de.monticore.lang.monticar.ema2kt.runtime.api.Component;
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentBuilder;
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters;
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentFactory;
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentMetaData;
import modelling.project1.meta.modelling.project1.component.test.sub2.MetaDataForAdd;
import modelling.project1.meta.modelling.project1.component.test.sub2.MetaDataForMult;
import org.jetbrains.annotations.NotNull;

public class MyComponentBuilder implements ComponentBuilder {

    private final ComponentBuilder builder;

    public MyComponentBuilder(ComponentBuilder builder) {
        this.builder = builder;
        ComponentFactory factory = builder.getFactory();
        factory.registerFactoryFunction(MetaDataForAdd.INSTANCE.getComponentId(), params -> new AddImplementation());
        factory.registerFactoryFunction(MetaDataForMult.INSTANCE.getComponentId(), params -> new MultImplementation());
    }

    @NotNull
    public ComponentFactory getFactory() {
        return builder.getFactory();
    }

    @NotNull
    public Component buildComponent(@NotNull ComponentMetaData meta, @NotNull ComponentCreationParameters params) {
        return builder.buildComponent(meta, ComponentCreationParameters.Companion.create());
    }

    public static MyComponentBuilder create() {
        return new MyComponentBuilder(ComponentBuilder.Companion.create());
    }
}
