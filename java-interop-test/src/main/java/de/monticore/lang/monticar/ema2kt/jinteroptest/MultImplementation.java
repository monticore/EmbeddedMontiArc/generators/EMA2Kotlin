/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.jinteroptest;

import modelling.project1.component.test.sub2.Mult;

public class MultImplementation extends Mult {
    public void execute() {
        Double in1 = getIn1();
        Double in2 = getIn2();
        if (in1 != null && in2 != null) {
            setOut1(in1 * in2);
        }
    }
}
