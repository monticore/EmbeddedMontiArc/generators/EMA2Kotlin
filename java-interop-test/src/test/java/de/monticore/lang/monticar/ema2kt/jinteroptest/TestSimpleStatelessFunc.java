/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.ema2kt.jinteroptest;

import de.monticore.lang.monticar.ema2kt.runtime.api.Component;
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters;
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentKt;
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentSystem;
import modelling.project1.meta.modelling.project1.component.test.sub2.MetaDataForSimpleStatelessFunc;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TestSimpleStatelessFunc {

    public static final double NUMERIC_PRECISION = 1e-5;

    final MyComponentBuilder builder = MyComponentBuilder.create();

    ComponentSystem componentSimpleStatelessFunc;

    @Before
    public void setup() {
        Component c = builder.buildComponent(MetaDataForSimpleStatelessFunc.INSTANCE, ComponentCreationParameters.Companion.create());
        componentSimpleStatelessFunc = ComponentKt.toComponentSystem(c);
    }

    @Test
    public void test() {
        Map<String, Object> inputs = new HashMap<String, Object>();
        inputs.put("in1", 1.0);
        inputs.put("in2", 2.0);
        inputs.put("in3", 3.0);
        inputs.put("in4", 4.0);
        inputs.put("in5", 5.0);
        Map<String, Object> outputs = componentSimpleStatelessFunc.execute(inputs);
        Assert.assertNotNull(outputs);
        Object result1 = outputs.get("out1");
        Assert.assertTrue(result1 instanceof Double);
        Double result2 = (Double) result1;
        double expectedResult = 19.0;
        Assert.assertEquals(expectedResult, result2, NUMERIC_PRECISION);
    }
}
